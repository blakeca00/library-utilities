#!/usr/bin/env bash
cd ~/git/MPCAWS-PROJECT

if [ ! "$1" ]; then
    printf -- "You must provide a version number as a search target\n"
else
    for i in $(find . -name "Jenkinsfile"); do
        echo $i
        grep "$1" $i
    done
fi

exit 0
