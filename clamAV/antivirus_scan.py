#!/usr/bin/env python3

#
#   Antivirus Scan | Linux
#

import os
import subprocess
from libtools import split_list


# large and/or complex directories that
# must be subdivided and scanned in parts
complex_directories = ['git', 'Documents']
home_dir = os.path.expanduser('~')


def source_target(parent):
    """
    Antivirus fs directory targets for Scanning

    Args:
        :parent (string): Parent directory; all scanning in subdirectories

    Returns:
        list of parent subdirectories
    """
    return os.listdir(parent)


def filter_complex_directories(complex_dir):
    """
    Subdivides source target into complex directories

    Args:
        :complex_dir (string): fs subdirectory of source target directory

    Returns:
        list of complex directory subdirectories, TYPE: list
    """
    num_partitions = 4
    #
    p1, p2, p3, p4 = split_list(complex_dir, num_partitions)
    return [p1, p2, p3, p4]


subdirs_git = filter_complex_directories(os.path.join(home_dir, 'git'))

subdirs_Documents = filter_complex_directories(os.path.join(home_dir, 'Documents'))
