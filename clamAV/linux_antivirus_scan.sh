#!/usr/bin/env bash

pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
clear=$(which clear)
date=$(date +"%Y-%m-%d @ %H:%M")
start_time=$(date +%s)
LOG_FILE="$HOME/logs/clamav.log"

# source after QUIET set
source $pkg_path/std_functions.sh
source $pkg_path/colors.sh

# formatting
frame=$(echo -e ${brightblue})
reset=$(tput sgr0)
bodytext=$(echo -e ${reset}${a_wgray})
btext=$(echo -e ${reset}${a_wgray})
sp="${frame}|${bodytext}"
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`
#
# ansi bright colors
a_brightgreen='\033[38;5;95;38;5;46m'                  # ansi brightgreen
wb=${BOLD}${white}                                     # white bold
lbkt="$(echo -e ${cyan}${BOLD})[${reset}"              # left arrow bracket
rbkt="$(echo -e ${cyan}${BOLD})]${reset}"              # right arrow bracket
lar="$(echo -e ${lgray}${BOLD})<${reset}"              # left arrow bracket
rar="$(echo -e ${lgray}${BOLD})>${reset}"              # right arrow bracket
vb="$(echo -e ${cyan}${BOLD})|${reset}"                # separator bar
orange=$(echo -e ${orange})                            # highlight color

# output to tty or not
echo "QUIET VAR IS:  $QUIET"
[ -z "$PS1" ] && echo "Not Interactive" || echo "Yes Interactive"


# --- declarations ------------------------------------------------------------


function help_menu(){
    cat <<EOM

             ${title}$pkg${btext} command help

  ${title}DESCRIPTION${btext}

            Scan HOME directory subdirectories:
                - ~/git
                - ~/Documents

  ${title}SYNOPSIS${btext}

        $ sh ${orange}$pkg${reset}  ${bbc}[${btext} --Documents ${bbc}]${btext} || ${bbc}[${btext} --git ${bbc}]${btext}

                       [-a | --all-jobs ]
                       [-d | --Documents ]
                       [-g | --git ]
                       [-h | --help  ]

  ${title}OPTIONS${btext}

        ${title}-d${btext}, ${title}--Documents${btext}  <value> :  If provided, scans
            the ~/Documents subdirectory in segments to conserve
            system memory use.

        ${title}-g${btext}, ${title}--git${btext}: If provided, $pkg scans
            the ~/git subdirectory in segments to conserve
            system memory use.

        ${title}-h${btext}, ${title}--help${btext}:  Display this help menu.

EOM
    #
    # <-- end function help_menu -->
}


function parse_parameters(){
    ##
    ##  Parse all command-line parameters
    ##

    local var command

    if [[ ! "$*" ]]; then

        help_menu
        exit 0

    else
        while [ $# -gt 0 ]; do
            case $1 in
                '-a' | '--all-jobs')
                    _check_prerequisites
                    scan_documents
                    scan_git
                    exit 0
                    ;;

                '-h' | '--help')
                    help_menu
                    exit 0
                    ;;

                '-d' | '-D' | '--documents' | '--Documents')
                    # Find or create log file and find document sources for both backups
                    _check_prerequisites
                    scan_documents
                    exit 0
                    ;;

                '-g' | '--git' | '-G')
                    # Find or create log file and find document sources for both backups
                    _check_prerequisites
                    scan_git
                    shift 1
                    exit 0
                    ;;

                *)
                    std_warn "You must provide a valid parameter or None"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}


function _check_prerequisites(){
    ## Test to ensure clam AV scanner installed and accessible ##
    #
    if [[ $(command -v clamscan) ]]; then
        return 0
    else
        exit 1
    fi
    #
    # <-- end function _prerequisites -->
}


function enable_logging(){
    if [[ -z "$PS1" ]]; then
    #if [[ -z $PS1 >/dev/null 2>&1 ]]; then
        # NOT interactive
        QUIET=false
        return 1
    else
        #  Interactive terminal
        QUIET=false
        return 0
    fi
}


function scan_documents(){
    ## scan ~/Documents folder in segments ##
    #
    # ensure directory exists; otherwise return
    if [[ ! -e "$HOME/Documents" ]]; then return 0; fi

    # clear terminal and locate to root of home dir
    $clear
    cd "$HOME" || std_error_exit "Failed to change to local HOME directory" "$E_DEPENDENCY"

    std_message "Beginning Virus scan of ~/Documents directory | $date" "INFO"

    std_message "Scanning A --> E letter range in $HOME/Documents directory" "INFO"
    #clamscan -i -r Documents/[A-E]*  Documents/[a-e]*
    clamscan -i -r Documents/[a-eA-E]*

    std_message "Scanning F --> I letter range in $HOME/Documents directory" "INFO"
    clamscan -i -r Documents/[F-I]*  Documents/[f-i]*

    std_message "Scanning J --> P letter range in $HOME/Documents directory" "INFO"
    clamscan -i -r  Documents/[J-P]*  Documents/[j-p]*

    std_message "Scanning Q --> Z letter range in $HOME/Documents directory" "INFO"
    clamscan -i -r Documents/[Q-Z]*  Documents/[q-z]*

    std_message "Virus scan of ~/Documents directory complete." "INFO"
    #
    # <-- end function scan_documents -->
}


function scan_git(){
    ## scan ~/git dir in segments ##
    #
    # ensure directory exists; otherwise return
    if [[ ! -e "$HOME/git" ]]; then std_error_exit "git dir missing - Exit" 1; return 0; fi

    # clear terminal and locate to root of home dir
    $clear
    cd "$HOME" || std_error_exit "Failed to change to local HOME directory" "$E_DEPENDENCY"

    std_message "Beginning Virus scan of ~/git directory. Skipping ${a_orange}.git${reset} system directories." "INFO"

    std_message "Scanning ${a_orange}A --> E${reset} letter range in $HOME/git directory" "INFO"
    clamscan -i -r git/[A-E]*  git/[a-e]* --exclude='.git'

    std_message "Scanning ${a_orange}F --> I${reset} letter range in $HOME/git directory" "INFO"
    clamscan -i -r git/[F-I]*  git/[f-i]* --exclude='.git'

    std_message "Scanning ${a_orange}J --> P${reset} letter range in $HOME/git directory" "INFO"
    clamscan -i -r [J-P]*  [j-p]* --exclude='.git'

    std_message "Scanning ${a_orange}Q --> Z${reset} letter range in $HOME/git directory" "INFO"
    clamscan -i -r git/[Q-Z]*  git/[q-z]* --exclude='.git'

    std_message "Virus scan of ~/git directory complete." "INFO"
    #
    # <-- end function scan_git -->
}


#
# MAIN  ------------------------------------------------------------------
#


# eval options
parse_parameters "$@"

# calc end time for message, exit
end_time=$(date +%s)
delta=$(( $(( $end_time - $start_time )) / 60 ))

std_message "Virus scanning job complete. | Run Time: $delta minutes." "INFO"

# return to starting dir
cd "$pkg_path"

exit 0
