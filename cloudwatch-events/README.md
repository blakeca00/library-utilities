# CloudWatch Events -- Test Event Library
## README

* * *

### Test Event json Schemas

The files in this library are json schemas of actual cloudwatch test events collected  
during debugging and testing various tooling and software components.  In order to  
utilize a given test event, simply search and replace the old resource ID's with  
new ones that are required to exist in your production environment.  

Test events greatly aid productivity by cutting both the cost and the time required  
for testing by eliminating creation and configuration of of actual, live AWS resources.  
Instead, resources are simulated by the test event when received by a target system  
for processing.  

* * *

### Use Cases

* Lambda function testing
* CloudWatch Alerts (various) Testing
* Database event Testing
* Testing any system which is required to respond to an event in an AWS env
