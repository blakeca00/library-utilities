#!/usr/bin/env bash


pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
NOW=$(date +'%Y-%m-%d')

# source dependencies
. "$pkg_path/std_functions.sh"
. "$pkg_path/colors.sh"


# trailing slash required for rsync to replicate director contents
source="$HOME/.aws"
target=$HOME/Backup/awscli/$NOW"_aws"
target_dir="$HOME/Backup/awscli"


std_message "Begin backup of .aws from ${a_brightblue}$source${resetansi} --> ${a_brightblue}$target${resetansi}" "INFO"

# make backup copy
cp -rv $source /tmp/
#cp -r "/tmp/.aws" "$target_dir/knowngood"
mv "/tmp/.aws" $target

std_message "Backup completed." "INFO"


sleep 2


std_message "Examining old backup copies for deletion" "INFO"

# id, but don'd delete files aged 5+ days
if [[ $(find "$target_dir" -type f -mtime +5) ]]; then

    std_message "Found the following files to delete:" "WARN"
    find "$target_dir" -type f -mtime +5

    # prompt to delete aged copies
    read -p "$(printf -- '\n\nDelete aged copies? [quit]:     \n\n')" choice

    if [ -z $choice ] || [ "$choice" = "q" ] || [ "$choice" = "quit" ]; then
        std_message "Exiting.  Retaining aged files." "INFO"
        exit 0
    else
        case $choice in
            'Yes' | 'yes' | 'Y' | 'y' | true)
                # delete files aged 5+ days
                std_message "DELETING FILES AGED 5 DAYS+." "INFO"
                find "$target_dir" -type f -mtime +5 -delete

                std_message "DELETING DIRECTORIES AGED 5 DAYS+." "INFO"
                find "$target_dir" -type d -mtime +5 -exec rm -rf {} \;
                
                std_message "FILE DELETION COMPLETED." "INFO"
                ;;
            *)
                # skip deletion
                std_message "Exiting.  Retaining aged files." "INFO"
                exit 0
        esac
    fi
else
    std_message "No aged files found for deletion.  Exit" "INFO"
fi
