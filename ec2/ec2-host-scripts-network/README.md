* * *
## README - Amazon ec2 host monitoring scripts (bash)
* * *

### Use

These are host-resident scripts for use on ec2 Linux virtual machines at AWS.

* * *

### Additional Info

[re:Invent 2015: ARC:401 - Black-Belt Networking for the Cloud Ninja](http://s3.amazonaws.com/reinvent-arc401/index.html)
