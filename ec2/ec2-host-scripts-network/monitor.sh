#!/bin/bash
HOSTNAME=`hostname`
INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
FILENAME=$INSTANCE_ID.%Y-%m-%d_%H:%M.pcap
mkdir -p ~/logs
cd ~/logs
cat << EOF | crontab
* * * * * ~/upload-logs.sh
EOF
sudo tcpdump -G 60 -w $FILENAME '(not src localhost and not dst port 443)'
