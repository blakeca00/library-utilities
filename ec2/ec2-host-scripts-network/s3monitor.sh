#!/bin/bash
INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
#echo $INSTANCE_ID
mkdir -p ~/logs
cd ~/logs
cat << EOF | crontab
* * * * * /usr/local/bin/aws s3 sync ~/logs s3://monitoringdemo/$INSTANCE_ID/ 
EOF
sudo tcpdump -G 60 -w 'trace_%Y-%m-%d_%H:%M.pcap' '(not src localhost and not dst port 443)'
