#!/bin/bash
HOSTNAME=`hostname`
INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
API_KEY=""
PACKET_CAPTURE_HOST=""
URL=http://$PACKET_CAPTURE_HOST/api/v1/$API_KEY/upload
PCAPs=~/logs

for f in $PCAPs/*.pcap
do
	#Check to see if file is still open before uploading
	TMP=`sudo lsof | grep $f`
        if [ -z $TMP ]; then
		# Only process file if it is not currently open
                echo "Processing $f"
		curl -F "file=@$f" -F "additional_tags=$INSTANCE_ID,$HOSTNAME" $URL 
		# File uploaded, so now remove it
                sudo rm $f
        fi
done
