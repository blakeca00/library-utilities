#!/bin/bash
HOSTNAME=`hostname`
INSTANCE_ID=`curl -s http://169.254.169.254/latest/meta-data/instance-id`
S3_BUCKET="monitoringdemo"
PCAPs=~/logs

for f in $PCAPs/*.pcap
do
	#Check to see if file is still open before uploading
	TMP=`sudo lsof | grep $f`
        if [ -z $TMP ]; then
		# Only process file if it is not currently open
                echo "Processing $f"
		/usr/local/bin/aws s3 cp $f s3://$S3_BUCKET/$INSTANCE_ID/ --quiet
		# File uploaded, so now remove it
                sudo rm $f
        fi
done
