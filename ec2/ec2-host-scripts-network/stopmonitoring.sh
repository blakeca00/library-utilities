#!/bin/bash

case $1 in
"--tag" | "--tags") 
	filter="tag-key"
	value=$2
	;;
"--group-id")
	filter="group-id"
	value=$2
	;;
"--group-name")
	filter="instance.group-id"
	value=(`aws ec2 describe-security-groups --group-names "$2" --region us-west-2 --output text | grep SECURITYGROUPS | cut -f 3`)
	;;
*)
	echo "Usage: $0 --tag TagName | --group-id SecurityGroupID | --group-name SecurityGroupName"
	exit
	;;
esac

#instancetxt=`aws ec2 describe-instances --filters "Name=$filter,Values=$value" --region us-west-2 --output text`
#echo $instancetxt
instance_ips=( `aws ec2 describe-instances --filters "Name=$filter,Values=$value" --region us-west-2 --output text | grep NETWORKINTERFACES | cut -f 6` )
for instance_ip in "${instance_ips[@]}"
do
	#echo $instance
	echo "Pushing monitor-off.sh to $instance_ip"
	~/scp_instance.sh $instance_ip monitor-off.sh .
	echo "Stopping monitoring on $instance_ip"
	~/ssh_instance.sh $instance_ip "./monitor-off.sh" >/dev/null 2>&1 &
done
