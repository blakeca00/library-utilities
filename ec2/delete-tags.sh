#!/usr/bin/env bash

# resets ec2 instances in test environment

# globals
pkg=$(basename $0)                                      # pkg (script) full name
pkg_root="$(echo $pkg | awk -F '.' '{print $1}')"       # pkg without file extention
pkg_path=$(cd $(dirname $0); pwd -P)                    # location of pkg

# error codes
E_BADARG=8                      # exit code if bad input parameter

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
gray=$(tput setaf 008)
lgray='\033[0;37m'              # light gray
dgray='\033[1;30m'              # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

#
# system functions  ------------------------------------------------------
#

# indent
function indent02() { sed 's/^/  /'; }
function indent10() { sed 's/^/          /'; }

function put_rule_help(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}$pkg${UNBOLD} --profile <${yellow}PROFILE${reset}>  --resources <${yellow}RESOURCE LIST${reset}> --region <${yellow}REGION${reset}>

            Resource list is comma separated list of resource ID's up to MAX of 3 resources:

                Example:

                    $pkg  --resources "i-0aae82128e8cc33ec,vol-056370b2fc6279ec4,eni-a6f42493"

            If omitted, REGION defaults to eu-west-1

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters(){
    if [[ ! $@ ]]; then
        put_rule_help
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -r | --resources)
                    RESOURCES="$2"
                    shift 2
                    ;;
                -p | --profile)
                    PROFILE="$2"
                    shift 2
                    ;;
                -r | --region)
                    REGION="$2"
                    shift 2
                    ;;
                -h | --help)
                    put_rule_help
                    shift 1
                    exit 0
                    ;;
                *)
                    echo "unknown parameter. Exiting"
                    exit $E_BADARG
                    ;;
            esac
        done
    fi
    # assignment
    if [ ! $REGION ]; then REGION="eu-west-1"; fi
    if [ ! $PROFILE ]; then PROFILE="default"; fi
    #
    # <-- end function parse_parameters -->
}

# -- main ------------------------------------------------------------------------------------------

parse_parameters "$@"

ARR_RESOURCES=$(echo $RESOURCES | awk -F ',' '{print $1" "$2" "$3}')

for resource_id in ${ARR_RESOURCES[@]}; do
    aws ec2 delete-tags --profile $PROFILE --region $REGION --resources $resource_id
    if [ $? -eq 0 ]; then
        printf -- "Tags from $resource_id deleted successfully\n"
    else
        printf -- "Problem deleting Tags from $resource_id\n"
    fi
done

exit 0
