* * *
# EC2 Utilities
* * *

## Summary

Library of useful bash utilities for [Amazon Web Services'](http://aws.amazonaws.com) Service.  If you do not have an account, [start here](https://aws.amazon.com) with AWS.

[back to the top](#markdown-header-ec2-utilities)

* * *

## Contents

* [Project Organization](#markdown-header-project-organization)
* [EC2 Instance Types](#markdown-header-get-ec2-instance-types) (Size) Report
* [Author & Copyright](#markdown-header-author-copyright)
* [License](#markdown-header-license)
* [Disclaimer](#markdown-header-disclaimer)

### Project Organization

Each directory is a separate project.  Each project encapsulates all of the required contents for a separate program or function.  Where possible, each directory will contain a project-specific README with the following sections:

1. Summary
2. Contents
3. Instructions
4. Identity and Access Management (IAM) Permissions
5. License
6. Disclaimer


### Get EC2 Instance Types

Generates report in realtime by downloading json data directly from AWS

![EC2 Instance Types](./get-ec2-instance-types/assets/20180210-ec2-sizes.png)


[back to the top](#markdown-header-ec2-utilities)

* * *

## Author & Copyright

All works contained herein copyrighted via below author unless work is explicitly noted by an alternate author.

* Copyright Blake Huber, All Rights Reserved.

[back to the top](#markdown-header-ec2-utilities)

* * *

## License

* Software contained in this repo is licensed under the respective license agreements in each project directory.  Generally, licensed under the [MIT License](https://opensource.org/licenses/MIT) unless noted otherwise.

[back to the top](#markdown-header-ec2-utilities)

* * *

## Disclaimer

*Code is provided "as is". No liability is assumed by either the code's originating author nor this repo's owner for their use at AWS or any other facility. Furthermore, running function code at AWS may incur monetary charges; in some cases, charges may be substantial. Charges are the sole responsibility of the account holder executing code obtained from this library.*

Additional terms may be found in the complete [license agreement](../LICENSE.md).

* * *

[back to the top](#markdown-header-ec2-utilities)

* * *
