#!/usr/bin/env bash

##
##  Create Custom ssh Public/ Private KeyPair (.pem)
##
##
##
##

BIT_DEFAULT="4096"
PRIVATE_DEFAULT="private_key.pem"
PUBLIC_DEFAULT="public_key.pem"
TMPDIR="/tmp"

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
orange='\033[38;5;95;38;5;214m'
gray=$(tput setaf 008)
wgray='\033[38;5;95;38;5;250m'                  # white-gray
lgray='\033[38;5;95;38;5;245m'                  # light gray
dgray='\033[38;5;95;38;5;8m'                    # dark gray
BOLD=`tput bold`
UNBOLD=`tput sgr0`
reset=$(tput sgr0)



# ---  declarations   ----------------------------------------------------------



function indent04() { sed 's/^/    /'; }


function std_error(){
    local msg="$1"
    #std_logger "$msg" "ERROR" $LOG_FILE
    echo -e "\n${yellow}[ ${red}ERROR${yellow} ]$reset  $msg\n" | indent04
}



function std_error_exit(){
    local msg="$1"
    local status="$2"
    std_error "$msg"
    exit $status
}



function binary_depcheck(){
    ## validate binary dependencies installed
    local check_list=( "$@" )
    local msg
    #
    for prog in "${check_list[@]}"; do
        if ! type "$prog" > /dev/null 2>&1; then
            msg="${title}$prog${reset} is required and not found in the PATH. Aborting (code $E_DEPENDENCY)"
            std_error_exit "$msg" $E_DEPENDENCY
        fi
    done
    #
    # <<-- end function binary_depcheck -->>
}


function std_message(){
    #
    # Caller formats:
    #
    #   Logging to File | std_message "xyz message" "INFO" "/pathto/log_file"
    #
    #   No Logging  | std_message "xyz message" "INFO"
    #
    local msg="$1"
    local prefix="$2"
    local log_file="$3"
    local format="$4"
    #
    if [ $log_file ]; then
        std_logger "$msg" "$prefix" "$log_file"
    fi
    [[ $QUIET ]] && return
    shift
    pref="----"
    if [[ $1 ]]; then
        pref="${1:0:5}"
        shift
    fi
    if [ $format ]; then
        echo -e "${yellow}[ $cyan$pref$yellow ]$reset  $msg" | indent04
    elif [ "$prefix" = "OK" ] || [ "$prefix" = "ok" ]; then
        echo -e "\n${yellow}[ $green${BOLD}$pref$yellow ]$reset  $msg\n" | indent04
    else
        echo -e "\n${yellow}[ $cyan$pref$yellow ]$reset  $msg\n" | indent04
    fi
}


# ---   main   -----------------------------------------------------------------


binary_depcheck "openssl"

cd $TMPDIR
std_message "Dependencies satisfied. Start Custom ssh keyfile generation" "INFO"


# determine bit level
printf -- "\n\n"
read -p "    Number of Key Bits  [4096]:    " choice
printf -- "\n\n"
if [ -z "$choice" ]; then 
    bits=4096
else
    bits=$choice
fi
std_message "Bit depth: $bits" "OK"


# private key info
printf -- "\n\n\tName of Private Key File?  [private_key.pem]:\n\n"
read -p "          >>    " private
printf -- "\n\n"
if [ -z $private ]; then
    PRIVATE_FILE=$PRIVATE_DEFAULT
else 
    PRIVATE_FILE=$private
fi
std_message "Private Key Filename:  $PRIVATE_FILE" "OK"


# public key info
printf -- "\n\n\tName of Public Key File?   [public_key.pem]:\n\n"
read -p "          >>    " public
printf -- "\n\n"
if [ -z $public ]; then
    PUBLIC_FILE=$PUBLIC_DEFAULT
else 
    PUBLIC_FILE=$public
fi
PUBLIC_FILE_AWS="$PUBLIC_FILE.aws"
std_message "Public Key Filename:  $PUBLIC_FILE" "OK"


# create keys
std_message "Creating Private Key file" "INFO"
openssl genpkey -algorithm RSA -out $PRIVATE_FILE -pkeyopt rsa_keygen_bits:$bits
std_message "Created $TMPDIR/$PRIVATE_FILE" "INFO"


# Create Matching Public Key
std_message "Creating matching public Key file" "INFO"
openssl rsa -pubout -in $PRIVATE_FILE -out $PUBLIC_FILE_TEMP
std_message "Created $TMPDIR/$PUBLIC_FILE" "INFO"

# strip header/footer from Public Key for import into AWS:
cat $PUBLIC_FILE | head -n -1 | tail -n +2 > $PUBLIC_FILE_AWS
std_message "Stripped header/footer to create $TMPDIR/$PUBLIC_FILE_AWS for import into AWS" "INFO"

std_message "Keyfiles created in $TMPDIR. Exit" "END"

exit 0

