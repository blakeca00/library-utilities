#!/usr/bin/env bash

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
orange='\033[38;5;95;38;5;214m'
gray=$(tput setaf 008)
lgray='\033[38;5;95;38;5;245m'    # light gray
dgray='\033[38;5;95;38;5;8m'      # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

# initialize default color scheme
accent=$(tput setaf 6)    # cyan
ansi=$(echo -e ${orange})   # use for ansi escape color codes


# --- declarations  ------------------------------------------------------------


# indent
function indent02() { sed 's/^/  /'; }
function indent04() { sed 's/^/    /'; }
function indent10() { sed 's/^/          /'; }
