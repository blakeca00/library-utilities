***
## EC2 Instance Type History
***

#### System files produced by [ec2-get-instancetypes.sh](../ec2-get-instancetypes.sh)

* Filenames begin with date of creation (refresh) by AWS, not the date added to this repo.
* File contents are a snapshot of the available AWS EC2 instances types as of the date of filename
* File format is plain (ASCII) text
