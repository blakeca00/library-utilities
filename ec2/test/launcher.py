"""
EC2 Test Machine Launcher
"""

import os
import json
import boto3
from botocore.exceptions import ClientError
from core import environment, loggers
#from pyaws.core import loggers
#from pyaws.ec2 import current_ami

logger = loggers.getLogger()

# globals
PROFILE = 'admin-sandbox'
IMAGE = 'ami-921423eb'      # amazonlinux 2
REGION = 'eu-west-1'
AMI_VERSION = 2
DEFAULT_REGION = os.getenv('AWS_DEFAULT_REGION', REGION)
session = boto3.Session(profile_name=PROFILE)
client = session.client('ec2', region_name=REGION)
ec2 = session.resource('ec2', region_name=REGION)


def amazonlinux1(profile, region=None, detailed=False, debug=False):
    """
    Return latest current amazonlinux v1 AMI for each region
    Args:
        :profile (str): profile_name
        :region (str): if supplied as parameter, only the ami for the single
        region specified is returned
    Returns:
        amis, TYPE: list:  container for metadata dict for most current instance in region
    """
    amis, metadata = {}, {}
    if region:
        regions = [region]
    else:
        regions = get_regions(profile=profile)

    # retrieve ami for each region in list
    for region in regions:
        try:
            client = boto3_session(service='ec2', region=region, profile=profile)
            r = client.describe_images(
                Owners=['amazon'],
                Filters=[
                    {
                        'Name': 'name',
                        'Values': [
                            'amzn-ami-hvm-2018.??.?.2018????-x86_64-gp2'
                        ]
                    }
                ])
            metadata[region] = r['Images'][0]
            amis[region] = r['Images'][0]['ImageId']
        except ClientError as e:
            logger.exception(
                '%s: Boto error while retrieving AMI data (%s)' %
                (inspect.stack()[0][3], str(e)))
            continue
        except Exception as e:
            logger.exception(
                '%s: Unknown Exception occured while retrieving AMI data (%s)' %
                (inspect.stack()[0][3], str(e)))
            raise e
    if detailed:
        return metadata
    return amis


def amazonlinux2(profile, region=None, detailed=False, debug=False):
    """
    Return latest current amazonlinux v2 AMI for each region
    Args:
        :profile (str): profile_name
        :region (str): if supplied as parameter, only the ami for the single
        region specified is returned
    Returns:
        amis, TYPE: list:  container for metadata dict for most current instance in region
    """
    amis, metadata = {}, {}
    if region:
        regions = [region]
    else:
        regions = get_regions(profile=profile)

    # retrieve ami for each region in list
    for region in regions:
        try:
            if not profile:
                profile = 'default'
            client = boto3_session(service='ec2', region=region, profile=profile)

            r = client.describe_images(
                Owners=['amazon'],
                Filters=[
                    {
                        'Name': 'name',
                        'Values': [
                            'amzn2-ami-hvm-????.??.?.2018????.?-x86_64-gp2',
                            'amzn2-ami-hvm-????.??.?.2018????-x86_64-gp2'
                        ]
                    }
                ])
            metadata[region] = r['Images'][0]
            amis[region] = r['Images'][0]['ImageId']
        except ClientError as e:
            logger.exception(
                '%s: Boto error while retrieving AMI data (%s)' %
                (inspect.stack()[0][3], str(e)))
            continue
        except Exception as e:
            logger.exception(
                '%s: Unknown Exception occured while retrieving AMI data (%s)' %
                (inspect.stack()[0][3], str(e)))
            raise e
    if detailed:
        return metadata
    return amis



aws ec2 --profile $ACCOUNT run-instances \
    --image-id 'ami-02ace471' \
    --subnet-id 'subnet-35029f6d' \
    --security-group-ids 'sg-76d96310' \
    --instance-type 't2.micro' \
    --region $REGION \
    --count "1" \
    --key-name 'managedOS-linux_ra1-dev' \
    --user-data file://testuserdata.sh > instance.json


#ami_image = amazonlinux2(profile=PROFILE, region=REGEION)[REGION]

client.run_instances(
    ImageId=IMAGE, InstanceType='t2.micro', SecurityGroupIds='default',
    SubnetId='subnet-67bb0900'
)
