#!/usr/bin/env bash

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
orange='\033[0;33m'
gray=$(tput setaf 008)
lgray='\033[0;37m'                  # light gray
dgray='\033[1;30m'                  # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

#
### -- functions declarations  ----------------------------------------------###
#

function help_menu(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}rds-delete-snapshot.sh${UNBOLD}

            -a | --accounts    <${yellow}ACCTFILE${reset}>
            -r | --type        <${yellow}REGIONCODE${reset}>
            -t | --type        manual | automated | shared | public
           [-s | --starttime   <${yellow}START_TIME${reset}> (Not implemented)]
           [-h | --help        Display help menu ]

                         ---

    ${BOLD}ACCTFILE${UNBOLD} :: text file containing iam roles to assume in all accounts
                to be profiled, one profile name per line

    ${BOLD}REGIONCODE${UNBOLD} :: AWS region code (example: us-east-1)

                         ---

    ${BOLD}Note${reset}  >>  gcreds is required to generate temporary credentials

EOM
    exit 0
    #
    # <<-- end function help_menu -->>
}

function parse_parameters(){
    if [[ ! $@ ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -a | --accounts)
                    ACCTFILE=$2
                    shift 2
                    ;;
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                -r | --region)
                    REGION=$2
                    shift 2
                    ;;
                -s | --starttime)
                    START_TIME=$2
                    shift 2
                    ;;
                -t | --type)
                    TYPE=$2
                    shift 2
                    ;;
                *)
                    help_menu
                    msg="Unrecognized argument [ $1 ] given. Exiting (code $E_BADARG)"
                    cis_error_exit "$msg" $E_BADARG
                    ;;
            esac
        done
    fi
    #
    # <<-- end function parse_parameters -->>
}

#
### -- main -----------------------------------------------------------------###
#

parse_parameters $@

for account in $(cat $ACCTFILE); do
    if $TYPE; then
        case $TYPE in
            'manual')
                # load array
                ct=0
                for snap-id in $(aws rds describe-db-snapshots \
                        --snapshot-type manual \
                        --profile gcreds-$account \
                        --region $REGION | \
                        jq -r .DBSnapshots[].DBSnapshotIdentifier)
                do
                    SNAPSHOT_IDS[$ct]="$snap-id"
                    ct=$(( $ct + 1 ))
                done
                ;;
            'automated')
                # load array
                ct=0
                for snap-id in $(aws rds describe-db-snapshots \
                        --snapshot-type automated \
                        --profile gcreds-$account \
                        --region $REGION | \
                        jq -r .DBSnapshots[].DBSnapshotIdentifier)
                do
                    SNAPSHOT_IDS[$ct]="$snap-id"
                    ct=$(( $ct + 1 ))
                done
                ;;
            'shared')
                # load array
                ct=0
                for snap-id in $(aws rds describe-db-snapshots \
                        --snapshot-type shared \
                        --profile gcreds-$account \
                        --region $REGION | \
                        jq -r .DBSnapshots[].DBSnapshotIdentifier)
                do
                    SNAPSHOT_IDS[$ct]="$snap-id"
                    ct=$(( $ct + 1 ))
                done
                ;;
            'public')
                # load array
                ct=0
                for snap-id in $(aws rds describe-db-snapshots \
                        --snapshot-type public \
                        --profile gcreds-$account \
                        --region $REGION | \
                        jq -r .DBSnapshots[].DBSnapshotIdentifier)
                do
                    SNAPSHOT_IDS[$ct]="$snap-id"
                    ct=$(( $ct + 1 ))
                done
                ;;
            *)
                echo -e "Unknown snapshot type -- exit"
                exit 0
                ;;
        esac
    elif $START_TIME; then
        echo "not yet implemented. exit"
        exit 0
        ct=0
        for snap-id in $(aws rds describe-db-snapshots \
                    --snapshot-type manual \
                    --profile gcreds-$account \
                    --region $REGION \
                    --query 'DBSnapshots[?SnapshotCreateTime<`2017-07-03`].[SnapshotCreateTime,DBSnapshotIdentifier]')
        #-------------------------------------------------------
        # NOTE: cmd will return all snapshot OLDER than 2017-07-03
        #-------------------------------------------------------
        do
            SNAPSHOT_IDS[$ct]="$snap-id"
            ct=$(( $ct + 1 ))
        done
    fi

    # delete rds snapshot
    for snap in ${SNAPSHOT_IDS[@]}; do
        aws rds delete-db-snapshot \
            --db-snapshot-identifier $snap \
            --profile gcreds-atos-gen-ra1-qa \
            --region eu-west-1 | jq .
        echo -e "\n"
    done
done

# <-- end -->
exit 0
