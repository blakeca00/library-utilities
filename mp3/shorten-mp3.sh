#!/usr/bin/env bash

input_file="$1"
output_file="$2"

printf -- '\n'
printf -- '%s\n' "Source file provided is: $input_file"
printf -- '%s\n' "Target file produced after reduction is: $output_file"
printf -- '\n'
read -p "Enter the length of file to be produced in seconds: " seconds

printf -- '\n%s\n' "Output file will be $seconds in duration"

read -p "Start encoding operation? [quit]: " choice

if [ -z "$choice" ] || [ "$choice" = "quit" ]; then
    exit 0
else
    ffmpeg -t $seconds  -i $input_file -acodec copy $output_file
fi

exit 0
