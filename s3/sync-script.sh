#!/usr/bin/env bash

# update s3 buckets in bulk from local

TARGET_DIR="~/Downloads/s3"

REGIONS=(
    "eu-west-1"
    "us-east-1"
    "ap-southeast-1"
)

ENV=(
    "qa"
    "pr"
)

#< -- start --

cd $TARGET_DIR

for region in ${REGIONS[@]}; do
    for environment in ${ENV[@]}; do

        # refresh buckets
        echo -e "\nUpdating Bucket: s3-$region-mpc-configuration-mgmt-$environment"
        aws s3 cp --recursive . "s3://s3-$region-mpc-configuration-mgmt-$environment" \
            --profile "gcreds-atos-tooling-$environment"

        # set public objects
        echo -e "\nUpdating Public ACL in region $region in tooling $environment"
        aws s3api put-object-acl \
            --acl public-read \
            --bucket "s3-$region-mpc-configuration-mgmt-$environment" \
            --key "linux/global_deps/jq-1.5" \
            --profile "gcreds-atos-tooling-$environment"

    done
done
