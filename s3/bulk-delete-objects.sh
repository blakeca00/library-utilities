#!/usr/bin/env bash

#
# used to bulk delete files of common extension (.pdf, .html) via aws api
#

bucket='s3-eu-west-1-reports'
profile='gcreds-atos-tooling-dev'
pattern='.html'

for obj in $(aws s3 ls s3://$bucket --profile $profile | awk '{print $4}'); do
    if [ $(echo $obj | grep "$pattern") ]; then
        echo -e "\ndeleting object: $obj"
        aws s3 rm s3://$bucket/$obj --profile $profile
    fi
done
