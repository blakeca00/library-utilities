#!/usr/bin/env python3
"""
Audit all S3 Buckets in an AWS Account. Totals size of each
bucket including grand total and associated bucket metadata.

"""

import os
import sys
import argparse
import boto3
from libtools import Colors, stdout_message, exit_codes

# colors
cm = Colors()
bdwt = cm.bd + cm.bwt
bdyl = cm.BRIGHT_YELLOW2 + cm.BOLD
reset = cm.RESET

profile = 'default'
bucket = 'spot-history'
tab2 = '\t'.expandtabs(2)
tab4 = '\t'.expandtabs(4)
global ALL_S3_BUCKETS

#---------------------------------- Subroutines --------------------------------


def _get_regions():
    client = boto3.client('ec2')
    return [x['RegionName'] for x in client.describe_regions()['Regions']]


def sum_list_contents(list):
    return sum(list)


def bucket_list():
    client = boto3.client('s3')
    return [x['Name'] for x in client.list_buckets()['Buckets']]


def bucket_size(bucket, quiet=False):
    # Bucket object
    bucket_resource = s3.Bucket(bucket)
    # reset counters
    total_bytes, size_byte, n = 0, 0, 0

    for bucket_object in bucket_resource.objects.filter():
        size_byte = size_byte + bucket_object.size
    return round(size_byte/1000/1024/1024, 2)


def bucket_regionsize(bucket, quiet=False):
    # Bucket object
    bucket_resource = s3.Bucket(bucket)
    bucket_total = 0

    for region in _get_regions():
        # reset counters
        total_bytes, size_byte, n = 0, 0, 0

        for bucket_object in bucket_resource.objects.filter(Prefix=region):
            size_byte = size_byte + bucket_object.size

        totalsize_GB = round(size_byte/1000/1024/1024, 2)
        bucket_total = bucket_total + totalsize_GB
    return round(bucket_total, 2)


def help_menu():
    """
    Displays help menu contents
    """
    menu_body = bdwt + """
      DESCRIPTION""" + cm.rst + """
                Profiles one or more S3 buckets to determine space allocation

        """ + bdwt + """
      SYNOPSIS""" + cm.rst + """
                  """ + synopsis_cmd + """

                                 -p, --profile    <value>
                                [-b, --bucket  <value> ]
                                [-a, --all  ]
                                [-h, --help    ]
                                [-V, --version ]
        """ + bdwt + """
      OPTIONS
            -p, --profile""" + cm.rst + """  <value>:  Profile  name of an IAM  (Identity Access
                Management) user from the local awscli configuration for which
                you want to rotate access keys.
        """ + bdwt + """
            -a, --all""" + cm.rst + """:  Analyze all S3 buckets and print report with results.
        """ + bdwt + """
            -V, --version""" + cm.rst + """:  Print the """ + PACKAGE + """ package version.
        """ + bdwt + """
            -h, --help""" + cm.rst + """:  Show this help message and exit.
        """
    print(menu_body)
    sys.stdout.write('\n')
    return


def init_cli():
    # process commandline args
    parser = argparse.ArgumentParser(add_help=False)

    try:

        args, unknown = options(parser)

    except Exception as e:
        help_menu()
        stdout_message(str(e), 'ERROR')
        sys.exit(exit_codes['E_BADARG']['Code'])

    if len(sys.argv) == 1 or args.help:
        help_menu()
        sys.exit(exit_codes['EX_OK']['Code'])

    elif args.profile:
        profile_name = args.profile

    elif args.bucket:
        bucket = args.bucket

    elif args.all:
        ALL_S3_BUCKETS = True

    elif len(sys.argv) == 2 and (sys.argv[1] != '.'):
        help_menu()
        sys.exit(exit_codes['EX_OK']['Code'])

    else:
        stdout_message(
            'Dependency check fail %s' % json.dumps(args, indent=4),
            prefix='AUTH',
            severity='WARNING'
            )
        sys.exit(exit_codes['E_DEPENDENCY']['Code'])

    failure = """ : Check of runtime parameters failed for unknown reason.
    Please ensure you have both read and write access to local filesystem. """
    logger.warning(failure + 'Exit. Code: %s' % sys.exit(exit_codes['E_MISC']['Code']))
    print(failure)



#---------------------------------- Statics ------------------------------------


# s3 resource object
s3 = boto3.resource('s3')

# Bucket object
bucket_resource = s3.Bucket(bucket)

# ec2 client object
session = boto3.Session(profile_name=profile)
client = boto3.client('ec2')

grand_total = 0
regions = []

# Print title header
print('\n' + tab2 + 'AWS S3 Bucket {}{}{} Keyspace storage'.format(bdyl, bucket, reset))
print('\n' + tab4 + '{: ^16} | {: ^8}'.format('Keyspace', 'Size (GB)'))
print(tab4 + '{: ^16} | {: ^8}'.format('-' * 16, '-' * 8))

for region in _get_regions():
    # reset counters
    total_bytes, size_byte, n = 0, 0, 0

    for bucket_object in bucket_resource.objects.filter(Prefix=region):
        size_byte = size_byte + bucket_object.size

    totalsize_GB = round(size_byte/1000/1024/1024, 2)
    regions.append(region)
    print('    {: <16} | {: >8}'.format(region, totalsize_GB))
    grand_total = grand_total + totalsize_GB

print('\n    TOTAL All Regions: {: >6} GB\n'.format(round(grand_total, 2)))
