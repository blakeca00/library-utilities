#!/usr/bin/env bash

#------------------------------------------------------------------------------
# AUTHOR:  Blake Huber
#
# SYNOPSIS:
#   - put s3 object multiple AWS accounts, multiple regions
#
# REQUIREMENTS:
#   - awscli
#   - jq (json parser)
#   - gcreds
#   - IAM roles in each account
#
# LICENSE:
#   - GPL v3
#------------------------------------------------------------------------------

# global vars
TMPDIR='/tmp'
IAM_PROFILE='A469005'           # profile in your local awscli config used to assume roles
CREDENTIALS="True"              # default is to generate temp credentials

declare -a s3buckets
s3buckets=(
    "s3-eu-west-1-configuration-mgmt-dev"
    "s3-eu-west-1-configuration-mgmt-qa"
    "s3-eu-west-1-configuration-mgmt-pr"
    "s3-us-east-1-configuration-mgmt-dev"
    "s3-us-east-1-configuration-mgmt-qa"
    "s3-us-east-1-configuration-mgmt-pr"
    "s3-ap-southeast-1-configuration-mgmt-dev"
    "s3-ap-southeast-1-configuration-mgmt-qa"
    "s3-ap-southeast-1-configuration-mgmt-pr"
    )

# error codes
E_BADARG=8            # exit code if bad input parameter
E_DEPENDENCY=1        # exit code if missing required dependency
E_EXPIRED_CREDS=9     # exit code if temporary credentials no longer valid

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
orange='\033[0;33m'
gray=$(tput setaf 008)
lgray='\033[0;37m'              # light gray
dgray='\033[1;30m'              # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

#
### -- system functions  ------------------------------------------------------#
#

# indent
indent02() { sed 's/^/  /'; }
indent10() { sed 's/^/          /'; }

function help_menu(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}bulk-s3-update.sh${UNBOLD}

            -k | --s3key     <${yellow}S3KEY${reset}>
            -o | --object    <${yellow}OBJECT${reset}>
            -c | --code      <${yellow}MFA_CODE${reset}>
           [-s | --skip      skip generation of temp credentials ]
           [-h | --help      display help menu ]

                         ---

      ${BOLD}S3KEY${UNBOLD} :: Amazon S3 directory names in the keyspace
               Example:  s3://mybucket/${yellow}Code${reset}/${yellow}repo1${reset}/myfile,
               S3KEY = "Code/repo1"

    ${BOLD}OBJECT${UNBOLD} :: file object on local disk

    ${BOLD}MFACODE${UNBOLD} :: code required by gcreds if cli authentication via mfa

                         ---

    ${BOLD}Note${reset}  >>  gcreds is required to generate temporary credentials

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters(){
    ## set input parameters ##
    local msg
    #
    if [[ ! $@ ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -k | --s3-key)
                    S3KEY=$2
                    shift 2
                    ;;
                -c | --code)
                    MFACODE=$2
                    shift 2
                    ;;
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                -s | --skip)
                    CREDENTIALS="False"
                    shift 1
                    ;;
                -o | --object)
                    OBJECT=$2
                    shift 2
                    ;;
                *)
                    help_menu
                    msg="Unrecognized argument [ $1 ] given. Exiting (code $E_BADARG)"
                    error_exit_ref_fcn "$msg" $E_BADARG
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}

function error_ref_fcn(){
    local msg="$1"
    #cis_logger "[ERRR]: $msg"
    echo -e "\n${yellow}[ ${red}ERRR${yellow} ]$reset  $msg\n" | indent02
}

function error_exit_ref_fcn(){
    local msg="$1"
    local status="$2"
    error_ref_fcn "$msg"
    exit $status
}

function generate_credentials(){
    # generate required temporary credentials for acct access
    localgcreds=$(which gcreds)
    $localgcreds -p atos-aua1 -a $ACCTFILE -c $MFACODE
    #
    # <<-- end function generate_credentials -->>
}

function valid_credentials(){
    ## check to ensure valid temp credentials ##
    local localgcreds=$(which gcreds)
    local msg
    #
    if [[ $($localgcreds -s | grep expired) ]] || \
       [[ $($localgcreds -s | grep "does not contain") ]]
    then
        msg="Temporary credentials have expired. Omit --skip parameter"
        error_exit_ref_fcn "$msg" $E_EXPIRED_CREDS
    else
        return 0
    fi
    #
    # <<-- end function valid_credentials -->>
}

function write_account_file(){
    declare -a profiles
    profiles=(
        "atos-tooling-dev"
        "atos-tooling-qa"
        "atos-tooling-pr"
        )

    ACCTFILE="$TMPDIR/tooling.accounts"
    # write file
    for acct in "${profiles[@]}"; do
        echo "$acct" >> $ACCTFILE
    done
    #
    # <--- end function write_account_file -->
}

#
### -- MAIN -------------------------------------------------------------------#
#

# <-- start -->

parse_parameters $@

if [ $CREDENTIALS == "True" ]; then
    # generate temp credentials, validate required parameters given
    if [ ! $OBJECT ]; then
        msg="OBJECT is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    elif [ ! $S3KEY ]; then
        msg="S3KEY is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    elif [ ! $MFACODE ]; then
        msg="MFACODE is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    fi
    # generate acocunt list file required by gcreds
    write_account_file
    # run gcreds
    generate_credentials

else
    # skip gen of temp credentials, validate required parameters given
    if [ ! $OBJECT ]; then
        msg="OBJECT is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    elif [ ! $S3KEY ]; then
        msg="S3KEY is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    fi
fi

if valid_credentials; then
    # upload to buckets across accounts
    for bucket in "${s3buckets[@]}"; do
        # select correct iam profile corresponding to bucket
        if [[ $(echo $bucket | grep "dev") ]]; then
            acct="atos-tooling-dev"
        elif [[ $(echo $bucket | grep "qa") ]]; then
            acct="atos-tooling-qa"
        elif [[ $(echo $bucket | grep "pr") ]]; then
            acct="atos-tooling-pr"        #statements
        else
            printf "s3 bucket in account not identified - Exit, code %s\n" $E_BADARG
            exit $E_BADARG
        fi

        # upload zip to bucket destination
        response="$(aws s3 cp $OBJECT s3://$bucket/$S3KEY/ --profile "gcreds-$acct")"
        source=$(echo $OBJECT)
        target=$(echo $response | awk -F ':' '{print $3}')

        # stdout messages
        echo -e "\nUpdating bucket ${blue}$bucket${reset} in account: ${BOLD}${white}$acct${UNBOLD}${reset}" | indent02
        echo -e "\nUploading:    ${BOLD}$source${UNBOLD}" | indent02
        echo -e "Destination:  ${BOLD}s3$target${UNBOLD}\n" | indent02
        echo -e "------------------------------------------------------------------------" | indent02
    done
fi
echo -e "\n"

if [ $CREDENTIALS == "True" ]; then
    # clean up, tempfiles
    rm $ACCTFILE
fi

# <-- end -->

exit 0
