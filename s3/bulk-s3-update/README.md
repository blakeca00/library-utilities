# README :  bulk-s3-update
* * *

## Purpose ##

**bulk-s3-update.sh** primary use case is put-object in multiple Amazon S3 buckets in multiple AWS  
accounts and regions of the world.  It can be used to push any file-type object to s3 buckets anytime  
automation is required.

**push-to-s3.sh** can be used with, or without **gcreds** temporary credential generator.  **gcreds** is  
only necessary if access to the AWS cli is protected by multi-factor authentication (MFA). If so, **gcreds**  
will first generate temporary credentials that temporarily bypass mfa requirements so that the script may  
update s3 buckets programmatically in rapid sequence.


* * *

## Deployment Owner/ Author ##

Blake Huber  
Slack: [@blake](https://mpcaws.slack.com/team/blake)  

* * *

## Contents ##

* [README.md](./README.md):  This file
* [bulk-s3-update.sh](./bulk-s3-update.sh):  utility script

* * *

## Dependencies ##

- One of the following python versions: 2.6.5, 2.7.X+, 3.3.X+, 3.4.X+
- Installation Amazon CLI tools (awscli, see Installation section)
- gcreds must be installed; found in PATH (only required if mfa required for cli access to AWS)
- bash (4.x)
- Standard linux utilities: grep, awk, sed, cat, hostname

* * *

## Usage ##

Help Menu

```bash
    $ sh bulk-s3-update.sh -h  
```

![help-menu](./images/help-menu.png)

* * *

## Output ##

**stdout** - when generating credentials

![output](./.images/stdout.png)


* * *
