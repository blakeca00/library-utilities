#!bin/bash
#_________________________________________________________________________
#                                                                         |
#                                                                         |
#  Author:   Blake Huber                                                  |
#  Purpose:  Audit all accessible buckets and log last date/time          |
#            bucket object was written.  This job is intended to be run   |
#            from cron to produce a report showing when backup jobs       |
#            uploaded backup artifacts to target buckets                  |
#  Name:     s3-bucket-audit.sh                                           |
#  Location: $EC2_REPO                                                    |
#  Requires: awscli                                                       |
#  Environment Variables (required, global):                              |
#      AWS_ACCESS_KEY      :   IAM User Private Key                       |
#      AWS_SECRET_KEY      :   IAM User Public Key                        |
#      AWS_DEFAULT_REGION  :   Home AWS region                            |
#                                                                         |
#  User:     $user                                                        |
#  Output:   logfile                                                      |
#  Error:    logfile                                                      |
#  Log:  $pkg_path/logs/                                                  |
#                                                                         |
#_________________________________________________________________________|

# vars
NOW=$(date -u +%s)      	    # this moment in epoch time
TODAY="$(date +"%Y-%m-%d")"   	# today's date
SCRIPTNAME=$(basename $0)       # name of this script with path removed
SNSREGION="us-east-1"           # region when accessing sns
SNSTOPIC="arn:aws:sns:us-east-1:$AWS_ACCT:admin"
PATH=/usr/local/bin:$PATH  	    # path to awscli, required when run from cron
JOBDIR=~/git/scripts-host       # path to scripts/ jobs
LOGFILE="~/logs/s3-audit-results.log"

# delay trigger settings
BACKUPDELAY=10                  # number of days allowed between web site backups
GLACIERDELAY=31                 # number of days allowed between vm backups
SPOTDELAY=10			        # number of days between spot price data pulls

# formatting
indent02() { sed 's/^/  /'; }
BOLD=`tput bold`
UNBOLD=`tput sgr0`

## set fs pointer to writeable temp location ##
if [ "$(df /run | awk '{print $1, $6}' | grep tmpfs 2>/dev/null)" ]; then
    # in-memory
    TMPDIR="/dev/shm"
    cd $TMPDIR
else
    TMPDIR="/tmp"
    cd $TMPDIR
fi

### function declaration start ###

function convert_time(){
    # time format conversion (http://stackoverflow.com/users/1030675/choroba)
    num=$1
    min=0
    hour=0
    day=0
    if((num>59));then
        ((sec=num%60))
        ((num=num/60))
        if((num>59));then
            ((min=num%60))
            ((num=num/60))
            if((num>23));then
                ((hour=num%24))
                ((day=num/24))
            else
                ((hour=num))
            fi
        else
            ((min=num))
        fi
    else
        ((sec=num))
    fi
    # echo "$day"d,"$hour"h,"$min"m
    echo "$day"d,"$hour"h
    #
    # <-- end function ec2cli-convert-time -->
    #
}

# time function to calc job duration
function timer()
{
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local  stime=$1
        etime=$(date '+%s')

        if [[ -z "$stime" ]]; then stime=$etime; fi

        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%d:%02d:%02d' $dh $dm $ds
    fi
}

### function declaration end ###

# <-- START -->

# Set start time:
START=$(timer)

# log file header
echo "------------ NEW RECORD --------------------"
echo ""
printf 'Job start time: '; date
echo ""

#
# loop thru all buckets, compare most recent object in bucket to alert
# threshold to determine when to trigger a job that is due (or notify as such)
#
for i in $(aws s3 ls | awk '{print $3}'); do
	#
	# grab date fields for each bucket.  NOTE: much better way
	# to do this would be to process entire json objects into array in memory
	#
	LASTMODIFIED=$(aws s3api list-objects --bucket "$i" --output json \
		| grep LastModified | tail -1 | awk '{print $2}' | cut -c 2-25)

	if [[ -z "$LASTMODIFIED" ]]; then
        # bucket empty
		echo "$i" >> empty_buckets.tmp
	else
		# bucket contains objects
		EPOCHTIME=$(date -d$LASTMODIFIED +%s)
		EPOCHDELTA=$(( $NOW-$EPOCHTIME ))
		DELTA=$(convert_time $EPOCHDELTA)

		# output results, truncate bucket name
		i=$(echo $i | cut -c 1-25)
		echo "$i" $DELTA >> .output.tmp

		### Alerts ###
		# trigger in seconds between web site backups
		DELAYSECS=$(( $BACKUPDELAY*24*60*60 ))
		if { [ $i == "backup-6yearsdown" ] || [ $i == "backup-ctodigest" ]; } && \
		[ $EPOCHDELTA -gt $DELAYSECS ]; then
			# send alert that backup overdue
			SUBJECT="Backup Job Triggered"
			echo bucket "$i" is OVERDUE for a backup job > msg.tmp
			echo Last backup was $DELTA ago. >> msg.tmp
            msg="\n[OVERDUE]: Backup overdue for archive bucket $i"
            echo $msg >> msg.tmp
			# aws sns to push msg to topic
		    aws sns publish --topic-arn $SNSTOPIC \
              		--region $SNSREGION \
              		--subject "$SUBJECT" \
		            --message file://msg.tmp
			rm msg.tmp    # clean up
		fi

        # trigger in seconds for glacier archives
        DELAYSECS=$(( $GLACIERDELAY*24*60*60 ))
        if { [ $i == "glacier-virtualmachines" ] || [ $i == "glacier-evernote" ]; } && \
        [ $EPOCHDELTA -gt $DELAYSECS ]; then
            # send alert that backup overdue
            SUBJECT="Glacier Archive DUE"
            echo "bucket "$i" is OVERDUE for a backup job" > msg.tmp
            echo "Last glacier backup was $DELTA ago." >> msg.tmp
            echo -e "\nBackup OVERDUE for bucket $i. Sending SNS notification:"
            # aws sns to push msg to topic
            aws sns publish --topic-arn $SNSTOPIC \
                    --region $SNSREGION \
                    --subject "$SUBJECT" \
                    --message file://msg.tmp
            rm msg.tmp    # clean up
        fi

        # trigger in seconds between spot price data pulls
        DELAYSECS=$(( $SPOTDELAY*24*60*60 ))
        if [ $i == "spot-history" ] && [ $EPOCHDELTA -gt $DELAYSECS ]; then
            # send alert that trigger overdue
            SUBJECT="Spot Price Data Pull DUE"
            echo bucket "$i" is OVERDUE for a job > msg.tmp
            echo Last spot price history data pull was $DELTA ago. >> msg.tmp
            echo -e "\nSpot price data pull OVERDUE for bucket $i. Sending SNS notification:"
            # aws sns to push msg to topic
            aws sns publish --topic-arn $SNSTOPIC \
                    --region $SNSREGION \
                    --subject "$SUBJECT" \
                    --message file://msg.tmp
            rm msg.tmp    # clean up
        fi
	fi
done

# print header
echo -ne "BucketName  LastModified\n -------------------------  ------------\n" | \
    awk '{ printf "%-27s %-20s \n", $1, $2}' | indent02 > .snsreport.tmp

# print output
awk '{ printf "%-27s %-20s \n", \
            $1, $2}' .output.tmp | sort -k +2n | indent02 >> .snsreport.tmp
echo -e "\nThe following buckets contained 0 objects (are empty):" >> .snsreport.tmp
cat empty_buckets.tmp | indent02 >> .snsreport.tmp

# display report to stdout for logging
cat .snsreport.tmp

#
# send final SNS report
#
# aws sns to push msg to topic
aws sns publish --topic-arn $SNSTOPIC \
    --region $SNSREGION \
    --subject "s3 Audit Results for $TODAY" \
    --message file://.snsreport.tmp > /dev/null

# Print script run time to stdout + mailmsg for SNS notification
#printf "$0 Job Complete. Run time: %s\n" $(timer $START) | tee .mailmsg.tmp
printf "\n\n${BOLD}$SCRIPTNAME${UNBOLD} run time: %s\n" $(timer $START)
printf "Ending Time: ""$(date)\n"
echo ""

# clean up
rm empty_buckets.tmp .output.tmp .snsreport.tmp

# <-- end -->
exit 0
