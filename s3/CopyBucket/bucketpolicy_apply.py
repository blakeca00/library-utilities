#!/usr/bin/env python3

import os
import sys
import json
import re
import fileinput
import logging
from time import sleep
from shutil import copy2 as copyfile
from botocore.exceptions import ClientError
from pyaws.session import boto3_session, _profile_prefix
from pyaws.utils import export_json_object, stdout_message


logger = logging.getLogger('1.0')
logger.setLevel(logging.INFO)

USE_FILES = False
SRC_DIR = os.environ.get('HOME') + '/Downloads'
POLICYTEMPLATE = os.getcwd() + '/' + 'policy.json'
TARGETROLE = 'apprenda-rnd'
OUTPUT_DIR = os.getcwd() + '/' + 'output'


s3client = boto3_session(service='s3', profile=_profile_prefix(TARGETROLE))


def preflight():
    """ Clean up previous run """
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
        stdout_message(f'Created {OUTPUT_DIR}')

    if len(os.listdir(OUTPUT_DIR)) > 0:
        for fobj in os.listdir(OUTPUT_DIR):
            os.remove(OUTPUT_DIR + '/' + fobj)
    return True


def source_bucketlist():
    """
    Summary:
        Real-time discovery of s3 buckets
    Returns:
        valid bucket names containing only Amazon S3 legal chars
    """

    buckets = []

    try:
        for bucket in [x['Name'] for x in s3client.list_buckets()['Buckets']]:
            if re.findall(r'[A-Z]', bucket):
                stdout_message(f'Eliminating bucket {bucket} - contains illegal characters')
                continue
            else:
                buckets.append(bucket)
    except ClientError as e:
        stdout_message(f'Error resulted when examinging bucket {bucket}: {e}', prefix='ERROR')
        sys.exit(1)
    return buckets


def get_bucketlist(filename):
    try:
        with open(filename) as f1:
            return f1.readlines()
    except OSError as e:
        logger.info(f'Problem opening file: {e}')


def update_policy_template(bucketname, template):
    """
    Customizes each bucket policy with required target bucket name
    """
    newfile = OUTPUT_DIR + '/' + bucketname.strip() + '.json'
    # create a copy
    copyfile(template, newfile)
    # customize for specific resource
    for line in fileinput.input([newfile], inplace=True):
        print(line.replace('BUCKETNAME', bucketname.strip()), end='')
    return newfile


def update_policy(bucketname):
    return {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AddPerm",
                "Effect": "Allow",
                "Principal": {
                    "AWS": [
                        "arn:aws:iam::103524393872:root"
                    ]
                },
                "Action": ["s3:GetObject"],
                "Resource": "arn:aws:s3:::%s/*" % bucketname
            }
        ]
    }


for bucket in source_bucketlist():

    if USE_FILES:
        # filesystem policy source
        # update bucket policy
        custom_policy = update_policy_template(bucket, POLICYTEMPLATE)

        with open (custom_policy) as f1:
            export_json_object(json.loads(f1.read()))

        # write policy file: bucket.json
        with open(custom_policy) as f1:
            policy = json.loads(f1.read())

    else:
        # virtual policy source
        policy = update_policy(bucket)

    # apply bucket policy
    r = s3client.put_bucket_policy(
            Bucket=bucket,
            Policy=json.dumps(policy)
        )

    # status msg
    rcode = r['ResponseMetadata']['HTTPStatusCode']

    if rcode >= 200 and rcode < 300:

        stdout_message(
            message=f'Bucket policy updated for bucket {bucket} - Response: {rcode}',
            prefix='OK'
        )
        # display bucket policy applied as a check
        stdout_message(f'Bucket policy applied to {bucket}:', prefix='OK')
        export_json_object(s3client.get_bucket_policy(Bucket=bucket))

    else:
        stdout_message(
            message=f'Failure during Bucket policy update for bucket {bucket} - Response: {rcode}',
            prefix='FAIL'
        )

sys.exit(0)
