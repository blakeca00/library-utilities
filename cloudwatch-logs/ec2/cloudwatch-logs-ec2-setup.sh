#
#  Amazon CloudWatch Logs |  EC2 host setup & configuration
#

# global deps
logger=$(which logger)
REGION=$(aws configure get region 2>/dev/null)

if [ ! $REGION ]; then
    REGION='us-east-1'
fi

function std_logger(){
    local msg="$1"
    local type=stdout   # stdout || logfile
    #
    if [[ $type == "logfile" ]]; then
        if [[ ! $log_fname ]]; then
            echo "$pkg: failure to call std_logger, $log_fname location undefined"
            exit $E_DIR
        fi
        logger "$(date +'%b %d %T') - [INFO]: $host $pkg: $msg"
    fi
    echo -e "$(date +'%b %d %T') - [INFO]: $host $pkg: $msg"
}

std_logger "Downloading cw logs agent..."
curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O

std_logger "Downloading agent dependency pack..."
curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/AgentDependencies.tar.gz -O

std_logger "Unpacking dep pack..."
tar xvf 'AgentDependencies.tar.gz' -C /tmp/

std_logger "Running awslogs-agent-setup.py in region $REGION..."
sudo python ./awslogs-agent-setup.py --region $REGION --dependency-path '/tmp/AgentDependencies'

# enable service awslogs

if [ $(grep -i amazon /etc/os-release) ]; then
    # Amazon Linux host
    sudo chkconfig awslogs --levels 35 on
elif [ $(grep -i systemctl 2>/dev/null) ]; then
    sudo systemctl enable awslogs
fi
