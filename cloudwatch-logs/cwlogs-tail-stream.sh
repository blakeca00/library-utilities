#!/bin/bash

#------------------------------------------------------------------------------
#
# Source: 
#       - Based on work by tekwiz, github
#       - https://gist.github.com/964a3a8d2d84ff4c8b5288d9a703fbce.git
# Notes: 
#       - Ensure you set the region in $aws_cmd_opts (cw logs are regional)
#         location of log group and stream
#       - start_seconds_ago is the log history displayed at script start
#       - tails continuously (ctrl-C to quit)
#
#------------------------------------------------------------------------------

# vars
group_name='<log-group-name>'
stream_name='<log-stream-name>'
start_seconds_ago=3600
aws_cmd_opts="--profile <profile-name> --region <region-code>"

# <-- functions -->

# Usage: get_loglines "<log-group-name>" "<log-stream-name>" <start-time>
get_loglines() {
    aws logs get-log-events $aws_cmd_opts \
        --output text \
        --log-group-name "$1" \
        --log-stream-name "$2" \
        --start-time $3
}

# Usage: get_next_start_time <prev-start-time> "<loglines>"
get_next_start_time() {
    next_start_time=$( sed -nE 's/^EVENTS.([[:digit:]]+).+$/\1/ p' <<< "$2" | tail -n1 )
    if [[ -n "$next_start_time" ]]; then
        echo $(( $next_start_time + 1 ))
    else
        echo $1
    fi
}

#<-- start -->

start_time=$(( ( $(date -u +"%s") - $start_seconds_ago ) * 1000 ))

while [[ -n "$start_time" ]]; do
    loglines=$( get_loglines "$group_name" "$stream_name" $start_time )
    [ $? -ne 0 ] && break
    start_time=$( get_next_start_time $start_time "$loglines" )
    echo "$loglines"
    sleep 1
done

#<-- end -->

exit 0
