#!/bin/bash

#------------------------------------------------------------------------------
#
# Source:
#       - Based on work by tekwiz, github
#       - https://gist.github.com/964a3a8d2d84ff4c8b5288d9a703fbce.git
# Notes:
#       - Ensure you set the region in $aws_cmd_opts (cwlogs are regional)
#         location of log group
#       - start_seconds_ago is the log history displayed at script start
#       - tails continuously (ctrl-C to quit)
#
#------------------------------------------------------------------------------

# globals
pkg=$(basename $0)                                      # pkg (script) full name
pkg_root="$(echo $pkg | awk -F '.' '{print $1}')"       # pkg without file extention
pkg_path=$(cd $(dirname $0); pwd -P)                    # location of pkg

# error codes
E_BADARG=8                      # exit code if bad input parameter

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
gray=$(tput setaf 008)
lgray='\033[0;37m'              # light gray
dgray='\033[1;30m'              # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

#
# system functions  ------------------------------------------------------
#

# indent
function indent02() { sed 's/^/  /'; }
function indent10() { sed 's/^/          /'; }

function put_rule_help(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}$pkg${UNBOLD} --profile <${yellow}PROFILE${reset}>  --log-group <${yellow}LOG_GROUP${reset}> --region <${yellow}REGION${reset}>

            If omitted, REGION defaults to eu-west-1

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters(){
    if [[ ! $@ ]]; then
        put_rule_help
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -l | --log-group)
                    LOG_GROUP="$2"
                    shift 2
                    ;;
                -p | --profile)
                    PROFILE="$2"
                    shift 2
                    ;;
                -r | --region)
                    REGION="$2"
                    shift 2
                    ;;
                -h | --help)
                    put_rule_help
                    shift 1
                    exit 0
                    ;;
                *)
                    echo "unknown parameter. Exiting"
                    exit $E_BADARG
                    ;;
            esac
        done
    fi
    # assignment
    if [ ! $REGION ]; then REGION="eu-west-1"; fi
    group_name="$LOG_GROUP"
    start_seconds_ago=60
    aws_cmd_opts="--profile $PROFILE --region $REGION"
    #
    # <-- end function parse_parameters -->

}

# Usage: get_loglines "<log-group-name>" <start-time>
function get_loglines() {
    aws logs filter-log-events $aws_cmd_opts \
        --output text \
        --log-group-name "$1" \
        --interleaved \
        --start-time $2
}

# Usage: get_next_start_time <prev-start-time> "<loglines>"
function get_next_start_time() {
    next_start_time=$( sed -nE 's/^EVENTS.([^[:blank:]]+).([[:digit:]]+).+$/\2/ p' <<< "$2" | tail -n1 )
    if [[ -n "$next_start_time" ]]; then
        echo $(( $next_start_time + 1 ))
    else
        echo $1
    fi
}

#<-- start -->

parse_parameters $@

start_time=$(( ( $(date -u +"%s") - $start_seconds_ago ) * 1000 ))

while [[ -n "$start_time" ]]; do
    loglines=$( get_loglines "$group_name" $start_time )
    [ $? -ne 0 ] && break
    start_time=$( get_next_start_time $start_time "$loglines" )
    echo "$loglines"
    sleep 2
done

#<-- end -->

exit 0
