#!/bin/bash

NOW=$(date +%s)                 # now, epoch time
SLICE=600                       # size of log history slice to collect, 10 min 
start_time=$(($NOW - $SLICE))   # collection start point in history

# pull log
aws logs filter-log-events \
    --log-group-name '/aws/lambda/CFN-MPC-AUTOTAG-EC2-FunctionAutoTag-S4GWCHOSETX1' \
    --profile gcreds-atos-tooling-dev \
    --region eu-west-1 \
    --start-time $start_time > cloudtrail-export.txt

# reduce & filter to json event
cat cloudtrail-export.txt | jq -r .events[].message | sed '/^$/d' > cloudtrail-export.json

# clean up
rm cloudtrail-export.txt
