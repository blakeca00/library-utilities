CUR_DIR=$(realpath $(dir $(lastword $(MAKEFILE_LIST))))
MPCBUILDVERSION :=
REQUIREMENT = Code/requirements.txt
ZIPNAME = limit-monitor-codebase.zip
S3KEY = Code/ta-limit-monitor
CFN_TEMPLATE = ta-limit-monitor-template.yml
PYTHON3_PATH = $(shell which python3.6)
VIRTUALENV := $(shell which virtualenv)
AWS_REGION := eu-west-1
ACCESS_KEY :=
SECRET_KEY :=
TARGET_ENV := dev
CYG := linux

ifeq ($(CYG), cygwin)
$(eval VP := bin)
$(eval LIB := lib/python3.6)
else
$(eval VP := bin)
$(eval LIB := lib/python3.6)
endif

all: zip cft

pre-build:
	rm -rf $(CUR_DIR)/dist
	mkdir $(CUR_DIR)/dist

venv: $(CUR_DIR)/$(REQUIREMENT)
ifeq ($(CYG), cygwin)
	$(PYTHON3_PATH) -m venv $(CUR_DIR)/venv
else
	$(PYTHON3_PATH) -m venv $(CUR_DIR)/venv
endif
	$(CUR_DIR)/venv/$(VP)/pip3 install -r $(REQUIREMENT)

zip: pre-build venv
	cd $(CUR_DIR)/Code && zip -r $(CUR_DIR)/dist/$(ZIPNAME) *
	cd $(CUR_DIR)/venv/$(LIB)/site-packages && zip -ur $(CUR_DIR)/dist/$(ZIPNAME) requests*
	. $(CUR_DIR)/venv/bin/activate

cft:
	cp $(CUR_DIR)/cloudformation/*.yml $(CUR_DIR)/dist/.

deploy: venv $(CUR_DIR)/dist
	$(CUR_DIR)/venv/bin/pip3 install s3cmd
	$(eval S3CMD = $(CUR_DIR)/venv/bin/s3cmd --no-check-certificate --access_key="$(ACCESS_KEY)" --secret_key="$(SECRET_KEY)")
	$(eval S3_BUCKET_PREFIX = s3-$(AWS_REGION)-mpc-install)

	$(S3CMD) put $(CUR_DIR)/dist/$(ZIPNAME) s3://$(S3_BUCKET_PREFIX)-$(TARGET_ENV)/$(S3KEY)/$(ZIPNAME)
	$(S3CMD) put $(CUR_DIR)/dist/$(CFN_TEMPLATE) s3://$(S3_BUCKET_PREFIX)-$(TARGET_ENV)/CFT/$(CFN_TEMPLATE)

clean:
	rm -rf $(CUR_DIR)/dist
	rm -rf $(CUR_DIR)/venv
