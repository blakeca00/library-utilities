#!/usr/bin/env bash

# Local Makefile wrapper


# vars
pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
IAM_USER="atos-aua1"
export PYTHON_VERSION="python3.5"
MAKE=$(which make)
sh=$(which bash)
repo_root=$(git rev-parse --show-toplevel)


# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
orange='\033[0;33m'
gray=$(tput setaf 008)
lgray='\033[0;37m'                  # light gray
dgray='\033[1;30m'                  # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

### -- functions declarations  ----------------------------------------------###

function help_menu(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}cis-account-baseline-cli.sh${UNBOLD}

            -e | --env         < ${yellow}dev | qa | pr | all${reset} >
            -m | --makefile    <${yellow}FILE${reset}>
           [-t | --target      makefile target ]
           [-h | --help        Display help menu ]

                         ---

    ${BOLD}Note${reset}  >>  gcreds is required to generate temporary credentials

EOM
    exit 0
    #
    # <<-- end function help_menu -->>
}

function parse_parameters(){
    if [[ ! $@ ]]; then
        help_menu
        echo -e "\nYou must provide path to the makefile as a parameter or run in same directory"
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -e | --env)
                    if [ $2 == "dev" ]; then
                        environments=(
                            "dev"
                        )
                    elif [ $2 == "qa" ]; then
                        environments=(
                            "dev"
                            "qa"
                        )
                    elif [ $2 == "pr" ]; then
                        environments=(
                            "pr"
                        )
                    elif [ $2 == "all" ]; then
                        environments=(
                            "dev"
                            "qa"
                            "pr"
                        )
                    else
                        echo -e "\nEnvironment required\n"
                    fi
                    shift 2
                    ;;
                -m | --makefile)
                    MAKEFILE="$2"
                    shift 2
                    ;;
                -t | --target)
                    TARGET="$2"
                    shift 2
                    ;;
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                *)
                    help_menu
                    echo -e "\nEnvironment AND makefile parameters are both required\n"
                    ;;
            esac
        done
    fi
    # Prerequisites
    if [ ! ${environments} ] || [ ! $MAKEFILE ]; then
        echo -e "\nEnvironment AND makefile parameters are both required\n"
    fi
    #
    # <<-- end function parse_parameters -->>
}

### -- main -----------------------------------------------------------------###

parse_parameters $@

# global exports
if [ -e $repo_root/version.sh ]; then
    export MPCBUILDVERSION=$($sh $repo_root/version.sh)
fi
export AWS_REGION=$(aws configure get $IAM_USER.region)

echo -e "\nBuilding MPCBUILD: $MPCBUILDVERSION\n"

for environ in ${environments[@]}; do
    ACCESS_KEY="$(aws configure get gcreds-atos-tooling-$environ.aws_access_key_id)"
    SECRET_KEY="$(aws configure get gcreds-atos-tooling-$environ.aws_secret_access_key)"
    export ACCESS_KEY=$ACCESS_KEY
    export SECRET_KEY=$SECRET_KEY
    export TARGET_ENV=$environ
    # execute Makefile.
    $MAKE -f $MAKEFILE $TARGET PYTHON_VERSION=$PYTHON_VERSION \
        AWS_REGION="$AWS_REGION" \
        ACCESS_KEY="$ACCESS_KEY" \
        SECRET_KEY="$SECRET_KEY" \
        TARGET_ENV="$environ"
done

# NOTE: 4 env vars passed as parameters to make are not necessary
#$MAKE -f $MAKEFILE $TARGET PYTHON_VERSION=$PYTHON_VERSION \
#    AWS_REGION="$AWS_REGION" \
#    ACCESS_KEY="$ACCESS_KEY" \
#    SECRET_KEY="$SECRET_KEY" \
#    TARGET_ENV="$environ"


exit 0
