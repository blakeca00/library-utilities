#!/bin/bash

#------------------------------------------------------------------------------
# AUTHOR:  Blake Huber
#
# SYNOPSIS:
#   - script to enable / disable existing cloudwatch rules,
#     update cron schedule for each rule across multiple accounts
#
# REQUIREMENTS:
#   - awscli
#   - jq (json parser)
#   - ubuntu linux v14.04 or higher
#   - IAM roles in each account
#
# LICENSE:
#   - GPL v3
#------------------------------------------------------------------------------

# global vars
RULE='SecurityBaselineRule'     # name of rule to update
STATE='DISABLED'                 # ENABLED || DISABLED
CRONEXP='cron(30 11 * * ? *)'    # cron expression to apply
REGION='eu-west-1'              # region in which cw event is located

# error codes
E_BADARG=8                      # exit code if bad input parameter

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
gray=$(tput setaf 008)
lgray='\033[0;37m'              # light gray
dgray='\033[1;30m'              # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

#
# system functions  ------------------------------------------------------
#

# indent
function indent02() { sed 's/^/  /'; }
function indent10() { sed 's/^/          /'; }

function put_rule_help(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}update-cloudwatch-rule.sh${UNBOLD}  -a <${yellow}ACCTFILE${reset}>  -c <${yellow}MFA_CODE${reset}>

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters(){
    if [[ ! $@ ]]; then
        put_rule_help
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -a | --accounts)
                    ACCTFILE=$2
                    shift 2
                    ;;
                -c | --code)
                    MFACODE=$2
                    shift 2
                    ;;
                -h | --help)
                    put_rule_help
                    shift 1
                    exit 0
                    ;;
                *)
                    echo "unknown parameter. Exiting"
                    exit $E_BADARG
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}

#
# MAIN  ------------------------------------------------------------------
#

parse_parameters $@

# generate required temporary credentials for acct access
#localgcreds=$(which gcreds)
#localgcreds -p A469005 -a $ACCTFILE -c $MFACODE

echo -e "\nUpdating Rule for CloudWatch Event : ${BOLD}${white}$RULE${UNBOLD}${reset}" | indent10

for acct in $(cat $ACCTFILE); do
    echo -e "\nAccount: ${BOLD}${white}$acct${UNBOLD}${reset}" | indent02
    aws events put-rule \
        --name "$RULE" \
        --schedule-expression "$CRONEXP" \
        --state $STATE \
        --profile "gcreds-$acct" \
        --region eu-west-1 | jq .
done

echo -e "\nStatus for Rule : ${BOLD}${white}$RULE${UNBOLD}${reset}" | indent10

for acct in $(cat $ACCTFILE); do
    aws events describe-rule \
                    --name "$RULE" \
                    --profile "gcreds-$acct" \
                    --region $REGION > .rule.json
    CRONEXP=$(jq -r .ScheduleExpression .rule.json)
    STATE=$(jq -r .State .rule.json)
    echo -e "\nAccount: ${BOLD}${white}$acct${UNBOLD}${reset}" | indent02
    echo -e "Schedule: ${blue}$CRONEXP${reset}" | indent02

    # colorize output msg based on state
    if [ $STATE == 'DISABLED' ]; then
        echo -e "Rule State: ${red}$STATE${reset}" | indent02
    else
        echo -e "Rule State: ${green}$STATE${reset}" | indent02
    fi
done

# clean up
rm .rule.json

# <-- end -->
exit 0
