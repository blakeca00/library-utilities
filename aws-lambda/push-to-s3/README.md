# README :  push-to-s3
* * *

## Purpose ##

**push-to-s3.sh** primary use case pushing lambda deployment code packages up to  
Amazon S3 buckets in multiple regions of the world.  It can be used to push any  
file-type object to multiple s3 buckets anytime automation is required.

**push-to-s3.sh** can be used with, or without **gcreds** temporary credential  
generator.  **gcreds** is only necessary if access to the AWS cli is protected by  
multi-factor authentication (MFA). If so, **gcreds** will first generate temporary  
credentials that temporarily bypass mfa requirements so that the script may update  
s3 buckets programmatically in rapid sequence.


* * *

## Deployment Owner/ Author ##

Blake Huber  
Slack: [@blake](https://mpcaws.slack.com/team/blake)  

* * *

## Contents ##

* [README.md](./README.md):  This file
* [push-to-s3.sh](./push-to-s3.sh):  utility script

* * *

## Dependencies ##

- One of the following python versions: 2.6.5, 2.7.X+, 3.3.X+, 3.4.X+
- Installation Amazon CLI tools (awscli, see Installation section)
- gcreds must be installed; found in PATH (only required if mfa required for cli access to AWS)
- [jq](https://stedolan.github.io/jq), a json parser generally available from your distribution repo
- bash (4.x)
- Standard linux utilities: grep, awk, sed, cat, hostname

* * *

## Usage ##

Help Menu

```bash
    $ sh push-to-s3.sh -h  
```

![](./images/help-menu.png)

* * *

## Output ##

```bash

    $ sh push-to-s3.sh \

            --s3key Code/autotag \

            --zipfile autotag-codebase.zip \

            --code 123456            

```  

### gcreds
Generating temporary credentials

![gcreds](./images/cred-gen.png)


### s3 put objects
Uploading to multiple s3 buckets in multiple accounts

![](./images/s3-upload.png)


* * *
