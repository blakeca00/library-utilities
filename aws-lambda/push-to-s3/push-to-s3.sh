#!/usr/bin/env bash

#------------------------------------------------------------------------------
# AUTHOR:  Blake Huber
#
# SYNOPSIS:
#   - push lambda code up to regional buckets
#
# REQUIREMENTS:
#   - awscli
#   - jq (json parser)
#   - gcreds
#   - IAM roles in each account
#
# LICENSE:
#   - GPL v3
#------------------------------------------------------------------------------

# global vars
pkg=$(basename $0)
TMPDIR='/tmp'
IAM_PROFILE='A469005'           # profile in your local awscli config used to assume roles
CREDENTIALS="True"              # default is to generate temp credentials

declare -a s3_dev               # non-production s3 buckets
declare -a s3_prod              # production s3 buckets
declare -a s3buckets            # deployment buckets


s3_dev=(
    "s3-eu-west-1-mpc-install-dev"
    "s3-us-east-1-mpc-install-dev"
    "s3-ap-southeast-1-mpc-install-dev"
    )

s3_qa=(
    "s3-eu-west-1-mpc-install-qa"
    "s3-us-east-1-mpc-install-qa"
    "s3-ap-southeast-1-mpc-install-qa"
    )

s3_prod=(
    "s3-eu-west-1-mpc-install-pr"
    "s3-us-east-1-mpc-install-pr"
    "s3-ap-southeast-1-mpc-install-pr"
    )

s3_fwdproxy=(
    "s3-eu-west-1-mpc-fprxy-dev"
    "s3-eu-west-1-mpc-fprxy-qa"
    "s3-us-east-1-mpc-fprxy-dev"
    "s3-us-east-1-mpc-fprxy-qa"
    "s3-ap-southeast-1-mpc-fprxy-dev"
    "s3-ap-southeast-1-mpc-fprxy-qa"
    )

# error codes
E_BADARG=8            # exit code if bad input parameter
E_DEPENDENCY=1        # exit code if missing required dependency
E_EXPIRED_CREDS=9     # exit code if temporary credentials no longer valid

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
orange='\033[0;33m'
gray=$(tput setaf 008)
lgray='\033[0;37m'              # light gray
dgray='\033[1;30m'              # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`

#
### -- system functions  ------------------------------------------------------#
#

# indent
indent02() { sed 's/^/  /'; }
indent04() { sed 's/^/    /'; }
indent10() { sed 's/^/          /'; }

function help_menu(){
    cat <<EOM

  Help Contents
  -------------

    $  sh ${BOLD}push-code-to-s3buckets.sh${UNBOLD}

            -k | --s3key     <${yellow}S3KEY${reset}>
            -z | --zipfile   <${yellow}ZIPFILE${reset}>
            -c | --code      <${yellow}MFA_CODE${reset}>
           [-q | --qa        deploy to qa s3 buckets ]
           [-p | --prod      deploy to production s3 buckets ]
           [-f | --proxy     deploy forward proxy s3 buckets ]
           [-s | --skip      skip generation of temp credentials ]
           [-h | --help      display help menu ]

                         ---

      ${BOLD}S3KEY${UNBOLD} :: Amazon S3 directory names in the keyspace
               Example:  s3://mybucket/${yellow}Code${reset}/${yellow}repo1${reset}/myfile,
               S3KEY = "Code/repo1"

    ${BOLD}ZIPFILE${UNBOLD} :: zip format containing lambda code and dependencies

    ${BOLD}MFACODE${UNBOLD} :: code required by gcreds if cli authentication via mfa

                         ---

    ${BOLD}Note${reset}  >>  gcreds is required to generate temporary credentials

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters(){
    ## set input parameters ##
    local msg
    #
    if [[ ! $@ ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -k | --s3-key)
                    S3KEY=$2
                    shift 2
                    ;;
                -c | --code)
                    MFACODE=$2
                    shift 2
                    ;;
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                -f | --proxy)
                    FWDPROXY=True
                    shift 1
                    ;;
                -p | --prod)
                    PRODUCTION=True
                    shift 1
                    ;;
                -q | --qa)
                    QA=True
                    shift 1
                    ;;
                -s | --skip)
                    CREDENTIALS="False"
                    shift 1
                    ;;
                -z | --zipfile)
                    ZIPFILE=$2
                    shift 2
                    ;;
                *)
                    help_menu
                    msg="Unrecognized argument [ $1 ] given. Exiting (code $E_BADARG)"
                    error_exit_ref_fcn "$msg" $E_BADARG
                    ;;
            esac
        done
    fi
    # set s3 deployment buckets
    if [ $QA ]; then
        s3buckets=( "${s3_qa[@]}" )
    elif [ $PRODUCTION ]; then
        s3buckets=( "${s3_prod[@]}" )
    elif [ $PRODUCTION ] && [ $QA ]; then
        s3buckets=( "${s3_qa[@]}" "${s3_prod[@]}" )
    elif [ $FWDPROXY ]; then
        # non-production deployment
        s3buckets=( "${s3_fwdproxy[@]}" )
    else
        # non-production deployment
        s3buckets=( "${s3_dev[@]}" )
    fi
    #
    # <-- end function parse_parameters -->
}

function error_ref_fcn(){
    local msg="$1"
    #cis_logger "[ERRR]: $msg"
    echo -e "\n${yellow}[ ${red}ERRR${yellow} ]$reset  $msg\n" | indent02
}

function error_exit_ref_fcn(){
    local msg="$1"
    local status="$2"
    error_ref_fcn "$msg"
    exit $status
}

function generate_credentials(){
    # generate required temporary credentials for acct access
    local exitcode
    #
    localgcreds=$(which gcreds)
    $localgcreds -p atos-aua1 -a $ACCTFILE -c $MFACODE
    exitcode=$?
    if [ $exitcode -ne 0 ]; then
        msg="\n${white}${BOLD}$pkg${UNBOLD}${reset} exit, failure to gen temp credentials."
        error_exit_ref_fcn -e "$msg (Code $exitcode)\n"  $exitcode | indent04
    fi
    #
    # <<-- end function generate_credentials -->>
}

function valid_credentials(){
    ## check to ensure valid temp credentials ##
    local localgcreds=$(which gcreds)
    local msg
    #
    if [[ $($localgcreds -s | grep expired) ]] || \
       [[ $($localgcreds -s | grep "does not contain") ]]
    then
        msg="Temporary credentials have expired. Omit --skip parameter"
        error_exit_ref_fcn "$msg" $E_EXPIRED_CREDS
    else
        return 0
    fi
    #
    # <<-- end function valid_credentials -->>
}

function write_account_file(){
    declare -a profiles
    profiles=(
        "atos-tooling-dev"
        "atos-tooling-qa"
        "atos-tooling-pr"
        )

    ACCTFILE="$TMPDIR/tooling.accounts"
    # write file
    for acct in "${profiles[@]}"; do
        echo "$acct" >> $ACCTFILE
    done
    #
    # <--- end function write_account_file -->
}

#
### -- MAIN -------------------------------------------------------------------#
#

# <-- start -->

parse_parameters $@

if [ $CREDENTIALS == "True" ]; then
    # generate temp credentials, validate required parameters given
    if [ ! $ZIPFILE ]; then
        msg="ZIPFILE is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    elif [ ! $S3KEY ]; then
        msg="S3KEY is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    elif [ ! $MFACODE ]; then
        msg="MFACODE is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    fi
    # generate acocunt list file required by gcreds
    write_account_file
    # run gcreds
    generate_credentials

else
    # skip gen of temp credentials, validate required parameters given
    if [ ! $ZIPFILE ]; then
        msg="ZIPFILE is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    elif [ ! $S3KEY ]; then
        msg="S3KEY is a required parameter. Aborting (code $E_DEPENDENCY)"
        error_exit_ref_fcn "$msg" $E_DEPENDENCY
    fi
fi

if valid_credentials; then
    # upload to buckets across accounts
    for bucket in "${s3buckets[@]}"; do
        # select correct iam profile corresponding to bucket
        if [[ $(echo $bucket | grep "dev") ]]; then
            acct="atos-tooling-dev"
        elif [[ $(echo $bucket | grep "qa") ]]; then
            acct="atos-tooling-qa"
        elif [[ $(echo $bucket | grep "pr") ]]; then
            acct="atos-tooling-pr"        #statements
        else
            printf "s3 bucket in account not identified - Exit, code %s\n" $E_BADARG
            exit $E_BADARG
        fi

        # upload zip to bucket destination
        response="$(aws s3 cp $ZIPFILE s3://$bucket/$S3KEY/ --profile "gcreds-$acct")"
        source=$(echo $ZIPFILE)
        target=$(echo $response | awk -F ':' '{print $3}')

        # stdout messaging
        echo -e "\nUpdating bucket ${blue}$bucket${reset} in account: ${BOLD}${white}$acct${UNBOLD}${reset}" | indent02
        echo -e "\nUploading:    ${BOLD}$source${UNBOLD}" | indent02
        echo -e "Destination:  ${BOLD}s3$target${UNBOLD}\n" | indent02
        echo -e "------------------------------------------------------------------------" | indent02
    done
fi
echo -e "\n"

if [ $CREDENTIALS == "True" ]; then
    # clean up, tempfiles
    rm $ACCTFILE
fi

# <-- end -->

exit 0
