#!/usr/bin/env python3

# Run just after CI/CD pipeline push to an environment
# verifies the push via comparison of mtime to current
# current time for the following:
#   - CFT
#   - Code pkg
#
# in all 3 tooling accounts, in all regions.
# Creates a record of mtime comps

from __init__ import __version
import os
import sys
import datetime
import argparse
from gcreds import GCreds
from lambda_utils import get_regions
import loggers
import boto3
from botocore.exceptions import ClientError

# time
now = datetime.datetime.utcnow()
ctime = str(now.strftime("%Y-%m-%dT%H:%M:%SZ"))



if __name__ == '__main__':

    # execute something to check the release
    argparser = argparse.ArgumentParser(description='PDV - Post-Deployment Verification')
    argparser.add_argument("-c", "--code", help="Code package to verify", required=True)
    argparser.add_argument("-p", "--profile", help="Credential profile", required=True)
    argparser.add_argument('--region', help='AWS Region, defaults to eu-west-1', default='eu-west-1', required=False)
    args = argparser.parse_args()
    package = args.code
    profile = args.profile
    region = args.region or ''

    regions = get_regions()
