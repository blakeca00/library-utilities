#!/bin/bash

#amazon os:

AD_DNS_SERVER='10.10.12.4'
AD_DOMAIN_SHORT="domain"
AD_DOMAIN_FULL="${DOMAIN_SHORT}.us-west-2.compute.internal"
AD_ADMIN_USER="Administrator"
AD_ADMIN_PW="PW"

#Add to file /etc/sysconfig/network-scripts/ifcfg-eth0
echo "DNS1=${AD_DNS_SERVER}" >> /etc/sysconfig/network-scripts/ifcfg-eth0

#Install required packages
yum -y install samba-winbind samba-winbind-clients pam_krb5

#Replace sshd lines in /etc/ssh/sshd_config
sed -i '/^[[:space:]]*ChallengeResponseAuthentication/c\ChallengeResponseAuthentication yes' /etc/ssh/sshd_config
sed -i '/^[[:space:]]*PasswordAuthentication/c\PasswordAuthentication yes' /etc/ssh/sshd_config

#Add sudo permissions to domain admins in file /etc/sudoers
echo '%domain\ admins ALL=(ALL:ALL) ALL' >> /etc/sudoers

#Replace /etc/resolv.conf nameserver to AD one
echo "" > /etc/resolvconf/resolv.conf.d/original
echo "nameserver ${AD_DNS_SERVER}" > /etc/resolvconf/resolv.conf.d/base
resolvconf -u

#Configure system to authenticate using AD (samba, kerberos, winbind, pam.d)
authconfig \
--enablekrb5 \
--krb5kdc="${AD_DOMAIN_FULL^^}" \
--krb5adminserver="${AD_DOMAIN_FULL^^}" \
--krb5realm="${AD_DOMAIN_FULL^^}" \
--enablewinbind \
--enablewinbindauth \
--smbsecurity=ads \
--smbrealm="${AD_DOMAIN_FULL^^}" \
--smbservers="${AD_DOMAIN_FULL}" \
--smbworkgroup="${AD_DOMAIN_SHORT^^}" \
--winbindtemplatehomedir=/home/%U \
--winbindtemplateshell=/bin/bash \
--enablemkhomedir \
--enablewinbindusedefaultdomain \
--update

#Join domain
net ads join -U "${AD_ADMIN_USER}%${AD_ADMIN_PW}"

#Restart services
service winbind start
service sshd restart

#Add services to startup
chkconfig winbind on
