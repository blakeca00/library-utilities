* * *
# Active Directory Domain Join Scripts for Linux
* * *

## Summary

These scripts are used to join a linux host to a Microsoft Active Directory  
domain. 

* * *

## Amazon Linux (AML) / RedHat Enterprise Linux 6

* [Amazon Linux](./AML_RHEL6_ad.sh)

* * *

## Ubuntu Linux

* [Ubuntu Linux](./ubuntu_join_ad.sh)
