#ubuntu:

AD_DNS_SERVER='10.10.12.4'
AD_DOMAIN_SHORT="domain"
AD_DOMAIN_FULL="${DOMAIN_SHORT}.us-west-2.compute.internal"
AD_ADMIN_USER="Administrator"
AD_ADMIN_PW="PW"

#Add to file /etc/dhcp/dhclient.conf
echo 'supersede domain-name-servers ${AD_DNS_SERVER};' >> /etc/dhcp/dhclient.conf

#Install required packages
apt-get update
export DEBIAN_FRONTEND=noninteractive
apt-get -y install samba winbind krb5-user

#Overwrite file /etc/samba/smb.conf with content 
cat << EOF > /etc/samba/smb.conf
[global]
   workgroup = "${AD_DOMAIN_SHORT^^}"
   password server = "${AD_DOMAIN_FULL}"
   realm = "${AD_DOMAIN_FULL^^}"
   security = ads
   idmap config * : range = 16777216-33554431
   template homedir = /home/%U
   template shell = /bin/bash
   kerberos method = secrets only
   winbind use default domain = true
   winbind offline logon = false
EOF

#Replace lines in /etc/nsswitch.conf with content
sed -i '/^[[:space:]]*passwd:/c\passwd:         compat winbind' /etc/nsswitch.conf
sed -i '/^[[:space:]]*group:/c\group:          compat winbind' /etc/nsswitch.conf

#Append lines to /etc/pam.d/common-session
echo 'session     required      pam_mkhomedir.so skel=/etc/skel/ umask=0022' >> /etc/pam.d/common-session

#Replace sshd lines in /etc/ssh/sshd_config
sed -i '/^[[:space:]]*ChallengeResponseAuthentication/c\ChallengeResponseAuthentication yes' /etc/ssh/sshd_config
sed -i '/^[[:space:]]*PasswordAuthentication/c\PasswordAuthentication yes' /etc/ssh/sshd_config
echo "UseDNS no" >> /etc/ssh/sshd_config

#Replace content of /etc/krb5.conf
cat << EOF > /etc/krb5.conf
[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 dns_lookup_realm = false
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true
 rdns = false
 default_realm = "${AD_DOMAIN_FULL^^}"
[realms]
 "${AD_DOMAIN_FULL^^}" = {
  kdc = "${AD_DOMAIN_FULL}"
  admin_server = "${AD_DOMAIN_FULL}"
 }

[domain_realm]
 "${AD_DOMAIN_FULL}" = "${AD_DOMAIN_FULL^^}"
 ."${AD_DOMAIN_FULL}" = "${AD_DOMAIN_FULL^^}"
EOF


#Add sudo permissions to domain admins in file /etc/sudoers
echo '%domain\ admins ALL=(ALL:ALL) ALL' >> /etc/sudoers

#Replace /etc/resolv.conf nameserver to AD one
echo "" > /etc/resolvconf/resolv.conf.d/original
echo "nameserver ${AD_DNS_SERVER}" > /etc/resolvconf/resolv.conf.d/base
dhclient -r
dhclient
resolvconf -u

#Join domain
net ads join -U "${AD_ADMIN_USER}%${AD_ADMIN_PW}"

#Restart services
service ssh restart
service winbind restart

#Add services to startup
update-rc.d winbind defaults
