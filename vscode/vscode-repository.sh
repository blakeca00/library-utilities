#!/usr/bin/env bash

#  Install VS Code Editor Integrated Development Environment

# globals
TMPDIR="/tmp"

function add_repository(){
    local os="$1"
    #
    if [ $(echo $os | grep -i ubuntu) ]; then
        curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
        sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
        sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
    else
        sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
        sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
    fi
}


function linux_distro(){
    ## determine linux os distribution ##
    local os_major
    local os_release
    local os_codename
    declare -a distro_info

    if [ "$(which lsb_release)" ]; then
        distro_info=( $(lsb_release -sirc) )
        if [[ ${#distro_detect[@]} -eq 3 ]]; then
            os_major=${distro_info[0]}
            os_release=${distro_info[1]}
            os_codename=${distro_info[2]}
        fi
    else
        ## AMAZON Linux ##
        if [ "$(grep -i amazon /etc/os-release  | head -n 1)" ]; then
            os_major="amazonlinux"
            if [ "$(grep VERSION_ID /etc/os-release | awk -F '=' '{print $2}')" = '"2"' ]; then
                os_release="$(grep VERSION /etc/os-release | grep -v VERSION_ID | awk -F '=' '{print $2}')"
                os_release=$(echo $os_release | cut -c 2-15 | rev | cut -c 2-15 | rev)
            elif [ "$(grep VERSION_ID /etc/os-release | awk -F '=' '{print $2}')" = '"1"' ]; then
                os_release="$(grep VERSION /etc/os-release | grep -v VERSION_ID | awk -F '=' '{print $2}')"
                os_release=$(echo $os_release | cut -c 2-15 | rev | cut -c 2-15 | rev)
            else os_release="unknown"; fi

        ## REDHAT Linux ##
        elif [ $(grep -i redhat /etc/os-release  | head -n 1) ]; then
            os_major="redhat"
            os_release="future"

        ## UBUNTU, ubuntu variants ##
        elif [ "$(grep -i ubuntu /etc/os-release)" ]; then
            os_major="ubuntu"
            if [ "$(grep -i mint /etc/os-release | head -n1)" ]; then
                os_release="linuxmint"
            elif [ "$(grep -i ubuntu_codename /etc/os-release | awk -F '=' '{print $2}')" ]; then
                os_release="$(grep -i ubuntu_codename /etc/os-release | awk -F '=' '{print $2}')"
            else
                os_release="unknown"; fi

        ## distribution not determined ##
        else
            os_major="unknown"; os_release="unknown"
        fi
    fi
    # set distribution type in environment
    export OS_DISTRO="$os_major"
    std_logger "Operating system identified as Major Version: $os_major, Minor Version: $os_release" "INFO" $LOG_FILE
    # return major, minor disto versions
    echo "$os_major $os_release $os_codename"
}


# --- main --------------------------------------------------------------------

cd $TMPDIR

os_type="$(linux_distro | awk '{print $1}')"

# verify if vscode repo installed
if ! check_repository "$os_type"; then
    add_repository "$os_type"
fi

# install based on distro
if [ "$os_type" = "redhat" ] || [ "$os_type" = "amazonlinux" ]; then
    sudo yum -y update
    sudo yum -y install code

elif  [ "$os_type" = "ubuntu" ]; then
    sudo apt -y update
    sudo apt -y install code

elif [ "$(grep -i fedora /etc/os-release)" ]; then
    sudo dnf -y update    # update cache
    sudo dnf -y install code
fi
