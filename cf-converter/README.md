* * *
## Cloudformation Template Converter
* * *

## Summary

Template converter to convert json cloudformation templates to the new yaml language standard.

Works for most templates.

## Source

Flux7

