* * *
#  Library -- Utilities
* * *

## Contents

Library of cli utilities in [GNU Bash](https://www.gnu.org/software/bash/bash.html) for use via [Amazon Web Services'](http://aws.amazonaws.com) Service.  Each directory encapsulates all of the required contents for a separate function.  Where possible, each directory will contain a script-specific README with the following sections:

1. Summary
2. Source
3. Contents
4. Instructions
5. IAM Permissions

**Note**: A few utilities may be implemented via python or other related technologies.

* * *

## Author & Copyright

All works contained herein copyrighted via below author unless work is explicitly noted by an alternate author.

* Copyright Blake Huber, All Rights Reserved.

* * *

## License

* Software contained in this repo is licensed under the [license agreement](https://bitbucket.org/blakeca00/library-utilities/src/master/LICENSE.md).

* * *

## Disclaimer

*Code is provided "as is". No liability is assumed by either the code's originating author nor this repo's owner for their use at AWS or any other facility. Furthermore, running function code at AWS may incur monetary charges; in some cases, charges may be substantial. Charges are the sole responsibility of the account holder executing code obtained from this library.*

Additional terms may be found in the complete [license agreement](https://bitbucket.org/blakeca00/library-utilities/src/master/LICENSE.md).

* * *
