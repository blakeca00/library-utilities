"""
Summary:
    Create json file object from text file
"""
import os
import sys
import datetime
import json
import argparse
import inspect
from pygments import highlight, lexers, formatters
from core import loggers

logger = loggers.getLogger()
container = []
now = datetime.datetime.now().strftime("%Y-%m-%d")
input_file = 'repository.list'
output_file = 'repository-audit.json'
infile_path = os.environ['HOME'] + '/Backup/usr/' + input_file
outfile_path = os.environ['HOME'] + '/Backup/usr/' + output_file


def pretty_print(list):
    """ prints json tags with syntax highlighting """
    json_str = json.dumps(list, indent=4, sort_keys=True)
    print(highlight(
        json_str, lexers.JsonLexer(), formatters.TerminalFormatter()
        ))
    print('\n')
    return True


def main():
    """ Creates json file from text file input """
    try:
        with open(infile_path) as f1:
            f2 = f1.readlines()
    except OSError as e:
        logger.exception(f'{inspect.stack()[0][3]}: problem parsing file {input_file}')
        return False
    try:
        for line in f2:
            location = line.split(',')[0]
            path = '/'.join(line.split(',')[0].split('/')[:-1])
            repo = line.split(',')[1]
            if repo.endswith('\n'):
                repo = repo[:-1]
            container.append(
                {
                    "location": location,
                    "path": path,
                    "repo": repo
                }
            )
        # write output file
        with open(outfile_path, 'w') as w1:
            w1.write(json.dumps(container))

        pretty_print(container)
        return True
    except Exception as u:
        logger.exception(f'{inspect.stack()[0][3]}: problem parsing file {input_file}')
        return False


if __name__ == '__main__':
    sys.exit(main())
