#!/usr/bin/bash

MAJOR=$(git describe --tags `git rev-list --tags --max-count=1`)
MINOR=$(git symbolic-ref --short -q HEAD | shasum | cut -c1-3)
MINORB=$(git rev-parse HEAD | cut -c1-7)
MPCBUILDVERSION=$(echo ${MAJOR}_${MINOR}_${MINORB})
echo $MPCBUILDVERSION
