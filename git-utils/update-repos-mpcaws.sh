#!/usr/bin/env bash

# update a specific project's repos, assuming all are in TARGETDIR
pkg_path=$(cd $(dirname $0); pwd -P)
GIT=$(which git)
TARGETDIR="$HOME/git/MPCAWS-PROJECT"
OUTPUT_DIR="$HOME/Backup/usr"
NOW=$(date +%Y-%m-%d)
REPOLIST="$OUTPUT_DIR/mpcaws-repository.list"

# dependencies
source $pkg_path/core/std_functions.sh
source $pkg_path/core/colors.sh

# init
if [ ! -d $OUTPUT_DIR ]; then mkdirs -p $OUTPUT_DIR; fi
if [ -e $REPOLIST ]; then rm $REPOLIST; fi
touch $REPOLIST

# git branches to update
declare -a BRANCHES
BRANCHES=(
    "develop"
    "master"
)

# array container for {path: $path, repository: $repo}

# --- declarations ------------------------------------------------------------


function archive_list(){
    local list="$1"
    local oldlist
    #
    if [ -e "$OUTPUT_DIR/$list" ]; then
        oldlist="$list".old
        mv "$OUTPUT_DIR/$list" "$OUTPUT_DIR/$oldlist"
    fi
}

function build_repolist(){
    local repo_path="$1"
    repo="$($GIT remote -v | head -n 1 | awk '{print $2}')"
    echo "$PWD, $repo" >> $repo_path
    return 0
}

function update_repos(){
    ## updates all repos in target directory (all branches given as parameters)
    local target="$1"
    declare -a branches=("${!2}")
    #
    cd $target

    for i in $(ls -d */); do

        # git repo, enter and update
        cd $i

        if [ -d .git ]; then

            # records current repogg
            build_repolist "$REPOLIST"

            # update current branch
            std_message "Updating repo: ${yellow}"$(echo $i | rev | cut -c 2-100 | rev)"${bodytext}" "INFO"
            msg="$($GIT pull)"
            printf -- "\t\t%s\n" $msg

            # capture starting branch name
            branch_orig=$($GIT branch | grep "*" | awk '{print $2}')

            # pull down updates for core branches
            for branch in ${branches[@]}; do
                std_message "Updating repo: ${yellow}"$(echo $i | rev | cut -c 2-100 | rev)"${bodytext}, Branch: ${title}$branch${bodytext}" "INFO"
                msg="$($GIT checkout $branch 2>/dev/null)"
                printf -- "\t\t%s\n" $msg
                msg="$($GIT pull)"
                printf -- "\t\t%s\n" $msg
            done

            # return to orig branch
            msg="$($GIT checkout $branch_orig 2>/dev/null)"
            printf -- "\t\t%s\n" $msg

        fi

        # leave repo
        cd ..

    done
    
}



# -- main ----------------------------------------------------------------------

archive_list "$REPOLIST"

update_repos "$TARGETDIR" BRANCHES[@]

# -- start --- #

exit 0
# -- end --- #
