#!/usr/bin/env bash

#------------------------------------------------------------------------------
#   Author:  	Blake Huber
#   Purpose: 	update all git repos in target dir
#   Required:
#               - Set TARGETDIR to the local root dir of all repos
#               - Set GITLIST to dir/filename of master list of all local repos
#
#   Description:
#               - suggest run script manually due to use of 'git pull'
#                 ie, your local repos will be merged with remote
#               - script traverses down 2 levels of hierarchy
#                 searching for git repos to update.
#
#   Legend:     Pull Locations
#   -------     --------------
#       L0:     Level 0. Top level git repo in $TARGETDIR
#       L1:     Level 1. 2nd level git repo in $TARGETDIR (repo in subdir)
#               Level 1 Embedded.  Repo within a repo in $TARGETDIR
#------------------------------------------------------------------------------

# vars
HOST=$(hostname)
NOW=$(date +"%Y-%m-%d")
pkg_path=$(cd $(dirname $0); pwd -P)
TARGETDIR="$HOME/git"
GIT=$(which git)
PYTHON3=$(which python3)
OUTPUT_DIR="$HOME/Backup/usr"
REPOLIST="$OUTPUT_DIR/repository.list"	   # master list of all active git repos
E_NODIR=1	  # error code, no master target dir found
E_NOREPO=2    # error code, no repo found

# dependencies
source $pkg_path/core/std_functions.sh
source $pkg_path/core/colors.sh

# init
if [ ! -d $OUTPUT_DIR ]; then mkdir -p $OUTPUT_DIR; fi
if [ -f $REPOLIST ]; then rm $REPOLIST; fi
touch $REPOLIST

# array to hold exclusions
declare -a EXCLUDE_LIST
EXCLUDE_LIST=(  "MPCAWS-PROJECT"   )

declare -a container
declare -a directories
declare -a filtered_directories

# git branches to update
declare -a BRANCHES
BRANCHES=(
    "develop"
    "master"
)


# --- declarations --------------------------------------------------------------------------------


function build_repolist(){
    local repo_path="$1"
    repo="$($GIT remote -v | head -n 1 | awk '{print $2}')"
    echo "$PWD, $repo" >> "$repo_path"
    return 0
}


function help_menu(){
    echo -e "\n${lgray}${BOLD}Input format${UNBOLD}${reset}:\n" | indent02
    echo -e "$ ${BOLD}$pkg${UNBOLD}   ${blue}[${reset} --exclusions ${blue}]${reset}  ${blue}[${reset} --debug ${blue}]${reset}\n\n" | indent10
}


function parse_parameters(){
    if [[ ! $@ ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -d | --debug)
                    DEBUG=true
                    shift 1
                    ;;
                -E | --exclusions | --exclude | --skip)
                    ACTIVE_EXCLUDE=true
                    shift 1
                    ;;
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                *)
                    help_menu
                    msg="Unrecognized argument [ $1 ] given. Exiting (code 1)"
                    echo "$msg"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <<-- end function parse_parameters -->>
}


function timer()
{
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local  stime=$1
        etime=$(date '+%s')

        if [[ -z "$stime" ]]; then stime=$etime; fi

        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%d:%02d:%02d' $dh $dm $ds
    fi
}


function update_repos(){
    ## updates all repos in target directory (all branches given as parameters)
    local target="$1"
    declare -a branches=("${!2}")

    # git repo, enter and update
    cd $target

    if [ -d "$target/.git" ]; then

        # records current repo
        build_repolist "$REPOLIST"

        # update current branch
        current=$($GIT branch | grep '\*' | awk '{print $2}')
        std_message "Updating repo: ${yellow}$i${bodytext}, Current Branch: ${green}$current${bodytext}" "INFO"
        msg="$($GIT pull)"
        printf -- "\t\t%s\n" $msg

        # capture starting branch name
        branch_orig=$($GIT branch | grep '\*' | awk '{print $2}')

        # pull down updates for core branches
        for branch in "${branches[@]}"; do
            std_message "Updating repo: ${yellow}$i${bodytext}, Branch: ${title}$branch${bodytext}" "INFO"

            if [[ $($GIT branch | grep $branch) ]]; then
                msg="$($GIT checkout $branch 2>/dev/null)"
                printf -- "\t\t%s\n" $msg
                msg="$($GIT pull)"
                printf -- "\t\t%s\n" $msg
            else
                std_message "Skipping update of branch ${red}$branch${bodytext}. Not found in ${yellow}$i${bodytext} repository" "INFO"
            fi
        done

        # return to orig branch
        msg="$($GIT checkout $branch_orig 2>/dev/null)"
        printf -- "\t\t%s\n" $msg

    fi
}


# --- start ---------------------------------------------------------------------------------------


parse_parameters "$@"


if [[ ! -e $TARGETDIR ]]; then
    echo -e "\nTarget directory [$TARGETDIR] containing git repos not found\n"
    exit $E_NODIR
elif [ ! $HOME ]; then
    echo -e "\nNo HOME environment var identified [$HOME]. Required\n"
    exit 1
fi

cd $TARGETDIR || false

START=$(timer)

# locate .git dir
for filepath in $(find . -name ".git"); do
    path=$(echo ${filepath%%/.git})
    directories=( "${directories[@]}"  "$TARGETDIR${path#.}"  )
done


if [ "$ACTIVE_EXCLUDE" ]; then
    # Filter directories through exclusion list
    for i in "${directories[@]}"; do
        for keyword in "${EXCLUDE_LIST[@]}"; do
            if [ $(echo "$i" | grep $keyword) ]; then
                continue
            else
                filtered_directories=( "${filtered_directories[@]}" "$i" )
            fi
        done
    done
else
    filtered_directories=( "${directories[@]}" )
fi


if [ $DEBUG ]; then
    std_message "DEBUGMODE invoked. Printing repository directories" "DBUG"
    # print out repository directories found
    for i in "${filtered_directories[@]}"; do
        echo "$i"
    done
    exit 0
fi


# update git repository directory unless in exclusion list
for i in "${filtered_directories[@]}"; do

    # git repo found, update it
    update_repos "$i" BRANCHES[@]

done


# create json file object for recreation if needed
$PYTHON3 $pkg_path/repository-audit.py

# timer, ending stats
std_message "$(printf "${title}COMPLETED${bodytext} - run time: %s\n" $(timer $START))" "INFO"


# <-- end -->
exit 0
