# README - git utilities
* * * 

### [autoupdate-local-repos.sh](./autoupdate-local-repos.sh)

* utility for updating multiple git repositories on your local machine at once
* traverses down up to 2 levels, finds and updates any repositories in $TARGETDIR
* when run, produces a date/time stamped text file listing all repos found on local machine

* * * 

### git-secrets

* prevents accidental check-in of any access keys, secret keys, and other confidential
files matching specified regex
* triggered via commit hook for git

* * * 
#### branchdiff

![branchdiff](./assets/branchdiff.png)


![branchdiff2](./assets/branch-diff.png)
