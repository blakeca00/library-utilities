#!/usr/bin/env bash

# globals
pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
config_path="$HOME/.config/bash"
clear=$(command -v clear)

# Source dependent modules
source $config_path/colors.sh
source $config_path/std_functions.sh

# formatting
LOG_FILE=$HOME/logs/rclone.log
frame=$(echo -e ${brightblue})
reset=$(tput sgr0)
bodytext=$(echo -e ${reset}${a_wgray})
btext=$(echo -e ${reset}${a_wgray})
sp="${frame}|${bodytext}"
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`
#
# ansi bright colors
a_brightgreen='\033[38;5;95;38;5;46m'                  # ansi brightgreen
wb=${BOLD}${white}                                     # white bold
lbkt="$(echo -e ${cyan}${BOLD})[${reset}"              # left arrow bracket
rbkt="$(echo -e ${cyan}${BOLD})]${reset}"              # right arrow bracket
lar="$(echo -e ${lgray}${BOLD})<${reset}"              # left arrow bracket
rar="$(echo -e ${lgray}${BOLD})>${reset}"              # right arrow bracket
vb="$(echo -e ${cyan}${BOLD})|${reset}"                # separator bar
orange=$(echo -e ${orange})                            # highlight color

ORIGIN_DIR=$(pwd)
TMP_ROOT='/home/blake/Downloads'


# Set excecutables
rclone_bin=$(command -v rclone)
rsync_bin=$(command -v rsync)
BROADCAST=false

# aws variables
S3_BUCKET='backup-gdrive'
REGION='us-east-2'

# error codes
E_DEPENDENCY=1
E_BADARG=8                  # exit code if bad input parameter
                            # given profilename from local awscli configuration

# --- declarations ------------------------------------------------------------


function help_menu(){
    cat <<EOM

                      ${title}miniclone.sh${btext} command help

  ${title}DESCRIPTION${btext}

            Backup TIG and Static Portfolio Google Drive
            file repositories to local machine

  ${title}SYNOPSIS${btext}

        $ sh ${bin}$pkg${reset}  ${bbc}[${btext} --tig-backup <value> ${bbc}]${btext} || ${bbc}[${btext} --static-portfolio ${bbc}|${btext}

                       [-a | --all-jobs ]
                       [-s | --static-portfolio ]
                       [-t | --tig-backup ]
                       [-h | --help  ]
                       [-V | --version ]

  ${title}OPTIONS${btext}

        ${title}-t${btext}, ${title}--tig-backup${btext}  <value> :  When provided, parameter uses branch
            name provided for  <value> as the baseline code reference
            instead of the master branch  (DEFAULT branch reference).

        ${title}-s${btext}, ${title}--static-portfolio${btext}: If provided, $pkg backs up the
            Google Drive repository for the Static Porfolio
            reference file archive.

        ${title}-d${btext}, ${title}--debug${btext}:  Enable additional debug logging.

        ${title}-h${btext}, ${title}--help${btext}:  Display this help menu.

        ${title}-V${btext}, ${title}--version${btext}:  Show $pkg version & license information.

EOM
    #
    # <-- end function help_menu -->
}


function parse_parameters(){
    ##
    ##  Parse all command-line parameters
    ##

    local var command

    if [[ ! "$*" ]]; then

        help_menu
        exit 0

    else
        while [ $# -gt 0 ]; do
            case $1 in
                '-a' | '--all-jobs')
                    _prerequisites "false"
                    static_portfolio_backup
                    tig_backup
                    shift 1
                    ;;

                '-h' | '--help')
                    help_menu
                    shift 1
                    ;;

                '-s' | '--static' | '--static-portfolio')
                    ### STATIC PORTFOLIO Backup ##
                    _prerequisites "false"
                    static_portfolio_backup
                    shift 1
                    ;;

                '-t' | '--tig' | '--tig-backup')
                    ## TIG Backup ##
                    _prerequisites "false"
                    tig_backup
                    shift 1
                    ;;

                '--test' )
                    # Test compression and upload to AWS logic
                    create_tgz_archive $TMP_ROOT/$(date +"%Y-%m-%d")_TIG.tgz "$LOCAL_TIG_SOURCE"
                    exit 0
                    upload_2_aws
                    ;;

                *)
                    std_warn "You must provide a valid parameter or None"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}


#
# MAIN  ------------------------------------------------------------------
#

#parse_parameters "$@"
# clear screen
$clear

# check if GUI app running (must be shutdown for maintenance)
#std_message "Checking to see if SpiderOak GUI app closed" "INFO"

#if [[ $(ps | grep -i spideroak | grep -v grep) ]]; then
#    std_error_exit "SpiderOak GUI running... must be shutdown. Exiting." $E_DEPENDENCY
#else
#    std_message "Starting SpiderOak maintenace script" "OK"
#fi

read -p "$(printf -- '\n\tSpiderOak GUI running (must be shutdown)? [quit]:   \n\n' )" choice

if [ -z $choice ] || [ "$choice" = "q" ] || [ "$choice" = "quit" ] || [ "$choice" = "Y" ] || [ "$choice" = "y" ]; then
    std_warn "Exiting - GUI running"
    exit 0
else
    case "$choice" in
        'N' | 'No' | 'n' | 'no')
            std_message "Starting SpiderOak maintenace script - GUI not running." "OK" $LOG_FILE
            ;;
    esac
fi

# Purge deleted (old) backup files older than 1 year
std_warn "Purging deleted (old) backup files older than 1 year. Please stand by..."
SpiderOakONE --purge-deleted-items=365

# Vacuum out database voids left by the purge
std_message "Vacuum database voids left by the purge" "OK"
SpiderOakONE --vacuum

# Rebuild the database to compact & compress
std_message "Rebuilding reference database.  This may take a while.  Please stand by..." "OK"
SpiderOakONE --rebuild-reference-database

exit 0

##
