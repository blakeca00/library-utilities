#!/usr/bin/env bash

# source dependencies
. ~/.config/bash/colors.sh
. ~/.config/bash/std_functions.sh

# local variables
LOCAL_STAGING='/mnt/vm/backup'
START=$(timer)      # requires std_function.sh

# colors
org=${a_orange}
bg=${a_bluegray}
bw=${white}${BOLD}

#
# <------------------------ start ---------------------------------->
#

# all directories to sync'd are relative to HOME root
cd $HOME || std_error_exit "Could not cd to HOME directory. Exit"


std_message "Start rsync of local directories to ${url}/mnt/vm/backup${reset} location." "INFO"
std_message "Start time is ${a_brightyellow}$(date +"%H:%M")${reset}" "INFO"
std_warn "Note:  git directory is skipped"

std_message "rsync of directories AWS, Audibooks, Backup, Desktop, and Documents --> ${url}/mnt/vm/backup${reset}" "INFO"
rsync -arv AWS Audiobooks Backup Desktop Documents /mnt/vm/backup/ --progress --delete

std_message "rsync of directories PDF, Music, Pictures, ZKeys --> ${url}/mnt/vm/backup${reset}" "INFO"
rsync -arv PDF Music Pictures ZKeys /mnt/vm/backup/ --progress --delete

std_message "rsync of directories .config and other dotfiles -->  ${url}/mnt/vm/backup${reset}" "INFO"
rsync -arv  .aws .bashrc .bash_aliases .bash_completion.d .config .gcreds .pypirc /mnt/vm/backup/ --progress --delete

std_message "rsync to local mount pt completed." "OK"

# OFFSITE sync to AWS S3
std_message "Begin ${url}/mnt/vm/backup${RESET} sync to AWS S3, ${bw}Bucket${RESET}: ${bg}s3-disk-cache${RESET}" "INFO"

cd "$LOCAL_STAGING" || std_error_exit "Could not cd to $LOCAL_STAGING directory. Exit"
std_message "Current working directory is ${url}$(pwd)${RESET}" "OK"

# SYNC TO AWS S3 bucket s3-disk-cache
std_message "Sync directory ${org}AWS${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./AWS s3://s3-disk-cache/AWS

std_message "Sync directory ${org}Audiobooks${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./Audiobooks s3://s3-disk-cache/Audiobooks

std_message "Sync directory ${org}Backup${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./Backup s3://s3-disk-cache/Backup

std_message "Sync directory ${org}Desktop${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./Desktop s3://s3-disk-cache/Desktop

std_message "Sync directory ${org}Documents${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./Documents s3://s3-disk-cache/Documents

std_message "Sync directory ${org}PDF${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./PDF s3://s3-disk-cache/PDF

std_message "Sync directory ${org}Music${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./Music s3://s3-disk-cache/Music

std_message "Sync directory ${org}Pictures${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./Pictures s3://s3-disk-cache/Pictures

std_message "Sync directory ${org}Zkeys${RESET} --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync ./ZKeys s3://s3-disk-cache/ZKeys

std_message "Sync directory ${org}.config${RESET} --> ${bg}s3-disk-cache/.config${RESET}" "INFO"
aws s3 sync ./.config s3://s3-disk-cache/.config

std_message "Sync directory ${org}.aws${RESET} --> ${bg}s3-disk-cache/.aws${RESET}" "INFO"
aws s3 sync ./.aws s3://s3-disk-cache/.aws

std_message "Sync other dotfiles --> ${bg}s3-disk-cache${RESET}" "INFO"
aws s3 sync .bashrc .bash_aliases .pypirc s3://s3-disk-cache
aws s3 sync .bash_completion.d s3://s3-disk-cache/.bash_completion.d
aws s3 sync .gcreds s3://s3-disk-cache/.gcreds

# End, print cumulative run time
echo -e "\n\t${BOLD}Total job runtime${RESET}: ${a_brightyellow}$(timer $START)${reset} sec\n"

# Return to HOME origin
cd $HOME || std_error_exit "Could not cd to HOME directory. Exit"

##

exit 0
