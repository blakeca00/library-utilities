* * *
#  Atom IDE Updater -- Linux 
* * * 

## Author / License

- Blake Huber
- GPL v3

* * *

## Purpose

Script that updates atom.io IDE text editor on linux.

* Downloads from official source @ [http://atom.io](https://atom.io)
* Redhat and Ubuntu distro support
* Installation and Update support

* * *

## Contents

### [update-atom-ide-linux.sh](./update-atom-ide-linux.sh)


* * * 

