#!/usr/bin/bash

#----------------------------------------------------------------------------
#                                                                           |
# creates a backup or installs all atom packages in your IDE configuration  |
#                                                                           |
#----------------------------------------------------------------------------

HOST="$(hostname)"
pwd_path=$(pwd)
BACKUP_DIR="$HOME/Backup/usr"
PKG_DEFAULT="$PWD/atom_($HOST).pkgs"
TMP_FILE="/tmp/atom.tmp"
APM="$(which apm)"

# Format
yellow=$(tput setaf 3)
BOLD=`tput bold`
UNBOLD=`tput sgr0`
reset=$(tput sgr0)

# --- declarations ------------------------------------------------------------

# indent
function indent02() { sed 's/^/  /'; }
function indent04() { sed 's/^/    /'; }
function indent10() { sed 's/^/          /'; }

function help_menu(){
    cat <<EOM

                    Help Contents
                    -------------

  ${BOLD}SYNOPSIS${UNBOLD}

        $  sh ${BOLD}atom-pkg-config.sh${UNBOLD}

                -c | --create     <${yellow}PKG_LIST${reset}> [DEFAULT]
                -i | --install    <${yellow}PKG_LIST${reset}>

  ${BOLD}OPTIONS${UNBOLD}

      ${BOLD}-c | --create${UNBOLD}: Create a list of all packages installed
            in the local atom configuration.

      ${BOLD}-i | --install${UNBOLD}: Install all atom packages specified in
            a list in package list

          ${BOLD}PKG_LIST${UNBOLD}, Format: 1 pkg per line

                    atom-clock
                    autocomplete-python
                    busy-signal
                    file-icons
                    ${yellow}... etc${reset}

        If no PKG_LIST given for --create option, default is used:
        DEFAULT = ${yellow}$PKG_DEFAULT${reset}

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters() {
    if [ ! $@ ]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                -c | --create)
                    CREATE="true"
                    if [ $2 ]; then
                        PKG_FILE=$2
                        shift 2
                    else
                        # assume default (write)
                        shift 1
                    fi
                    ;;
                -i | --install)
                    INSTALL="true"
                    PKG_FILE=$2
                    shift 2
                    ;;
                *)
                    echo "unknown parameter. Exiting"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}

function create_pkg_list(){
    local pkg_list="$1"
    local default="$2"
    ##    ##
    $APM list --installed --bare | awk -F '@' '{print $1}' > "$TMP_FILE"

    # rm empty lines
    if [ $default ]; then
        sed '/^$/d' $TMP_FILE > $pkg_list
        echo -e "\nCreated atom.io package list:\n"
        cat $pkg_list
    else
        sed '/^$/d' $TMP_FILE > $pwd_path/$pkg_list
        echo -e "\nCreated atom.io package list:\n"
        cat $pwd_path/$pkg_list
    fi
    rm $TMP_FILE
    # output contents
}

function install_pkg_list() {
    ## install list of pkgs from file ##
    local pkg_list="$1"
    echo -e "\nInstalling the following packages from file:\n"
    cat $pkg_list
    # install
    for pkg in $(cat $pkg_list); do
        $APM install $pkg
    done
}

function std_error(){
    local msg="$1"
    #std_logger "[ERROR]: $msg"
    echo -e "\n${yellow}[ ${red}ERROR${yellow} ]$reset  $msg\n" | indent04
}

function std_error_exit(){
    local msg="$1"
    local status="$2"
    std_error "$msg"
    exit $status
}


# --- main ------------------------------------------------------------


if [ ! -d "$BACKUP_DIR" ]; then
    if ! mkdir -p $BACKUP_DIR; then
        std_error_exit "BACKUP_DIR FAILED TO CREATE. Exit"
    fi
fi

parse_parameters "$@"


if [ $CREATE ] && [ ! $PKG_FILE ]; then
    create_pkg_list $PKG_DEFAULT DEFAULT

elif [ $CREATE ] && [ $PKG_FILE ]; then
    create_pkg_list $PKG_FILE

elif [ $INSTALL ]; then
    install_pkg_list $PKG_FILE
fi

exit 0
