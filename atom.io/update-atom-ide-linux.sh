#!/usr/bin/env bash

pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
LATEST_URL='https://github.com/atom/atom/releases/latest'
TMPDIR='/tmp'
NOW=$(date +%s)
ALLOWABLE_AGE=$(( 24*60*60 ))    # 1 day in seconds

# external Sources
config_path="$HOME/.config/bash"

atom_io="$(which atom 2>/dev/null)"


# --- declarations ------------------------------------------------------------

# indent
function indent02() { sed 's/^/  /'; }
function indent10() { sed 's/^/          /'; }


_valid_version(){
    ##
    ## Regex check of version number provided by user input
    ##
    version="$1"
    case $version in
        ?.?.? | ?.??.?)
            return 0
            ;;
        *)
            std_message "Version number provided not valid format (X.Y.Z)" "WARN"
            exit 1
            ;;
    esac
}


function help_menu(){
    cat <<EOM

                    Help Contents
                    -------------

  ${title}SYNOPSIS${rst}

        $  sh ${title}$pkg${rst}  --show  ${bcy}|${rst} --update  ${bcy}[${rst}{VERSION}${bcy}]${rst}

                -s | --show     show latest version available
                -u | --update   update atom ${yellow}[${reset}DEFAULT${yellow}]${reset}
                -h | --help     display this menu

  ${title}EXAMPLES${rst}

        ${gbl}Compare installed atom.io version to latest available:${rst}
            $ $pkg  --show

        ${gbl}Upgrade atom.io to the lastest version:${rst}
            $ $pkg  --update

        ${gbl}Upgrade atom.io to a specified version:${rst}
            $ $pkg  --update 1.41.0

EOM
    #
    # <-- end function put_rule_help -->
}

function parse_parameters() {
    if [[ ! "$@" ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                -s | --show)
                    SHOW_VERSION="true"
                    shift 1
                    ;;
                -u | --update)
                    if [ "$2" ]; then
                        UPDATE_ATOM="true"
                        VERSION="$2"
                        shift 2
                    else
                        UPDATE_ATOM="true"
                        VERSION="latest"
                        shift 1
                    fi
                    ;;
                *)
                    echo "unknown parameter. Exiting"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}


function clean_up(){
    ## removes downloads > allowable age ##
    if [ -e $TMPDIR/$TARGET ]; then
        LOCALDATE=$(stat -c '%Y' "$TMPDIR/$TARGET")
        if [ $(( $NOW - $LOCALDATE )) -gt $ALLOWABLE_AGE ]; then
            rm -f $TMPDIR/$TARGET
        fi
    fi
    return 0
}

function reported_version(){
    ## retrieve latest version of atom.io via public web pages ##
    local url="$1"
    local version
    local cur_ver
    #
    url="$(curl $url -s -L -I -o /dev/null -w '%{url_effective}')"
    version="$(echo $url | awk -F "/" '{print $NF}' | cut -c 2-30)"
    cur_ver="$(atom --version 2>/dev/null | head -n 1 | awk -F ':' '{print $2}')"
    if [ ! $cur_ver ]; then
        std_message "Atom Editor is not currently installed. ${ORANGE}atom${reset} editor version ${title}$version${rst} is available." "INFO"
    else
        std_message "${ORANGE}atom${reset} version ${title}$version${rst} is available. Version${title}$cur_ver${rst} is currently installed." "INFO"
    fi
    return 0
}

function pre_update(){
    ## validate next version available via direct download of binary ##
    local cur_ver
    local version="$1"
    #
    if [[ $(grep -i ubuntu /etc/os-release) ]]; then
        # header
        echo -e "\n Checking atom version available for ${ORANGE}${BOLD}ubuntu${reset} \n"
        # set to ubuntu format
        TARGET='atom-amd64.deb'
        # rm old file, if exists
        clean_up
        # download atom, latest
        if [ -e $TMPDIR/$TARGET ]; then
            std_message "Found $TMPDIR/$TARGET less than allowable age.  Using." "INFO"

        elif [[ "$version" ]]; then
            curl -L "https://github.com/atom/atom/releases/download/v${version}/$TARGET" > $TMPDIR/$TARGET
        else
            curl -L https://atom.io/download/deb > $TMPDIR/$TARGET
        fi
        version=$(dpkg -I $TMPDIR/$TARGET | grep Version | awk '{print $2}')

    elif [[ $(grep -i redhat /etc/os-release) ]]; then
        # header
        echo -e "\nChecking atom version available for ${BOLD}${RED}redhat${reset}\n"
        # set to rh format
        TARGET='atom.x86_64.rpm'
        # rm old file, if exists
        clean_up
        # download atom, latest
        if [ -e $TMPDIR/$TARGET ]; then
            std_message "Found $TMPDIR/$TARGET less than allowable age.  Using." "INFO"

        elif [[ "$version" ]]; then
            curl -L "https://github.com/atom/atom/releases/download/v${version}/$TARGET" > $TMPDIR/$TARGET

        else
            curl -L https://atom.io/download/rpm > $TMPDIR/$TARGET
        fi
        # Update atom from downloaded
        version=$(rpm -qip $TMPDIR/$TARGET 2>/dev/null | grep Version | awk -F ':' '{print $2}')
    else
        echo -e "\n unidentified distro detected. Aborting \n"
        exit 1
    fi

    # check current version installed; if any
    cur_ver="$(atom --version 2>/dev/null | head -n 1 | awk -F ':' '{print $2}')"
    if [ ! $cur_ver ]; then
        std_message "Atom Editor is not currently installed. ${ORANGE}atom${reset} editor version ${title}$version${rst} is available." "INFO"
    else
        std_message "${ORANGE}atom${reset} version ${title}$version${rst} is available. Version${title}$cur_ver${rst} is currently installed." "INFO"
    fi

    # proceed with installation
    read -p "    Do you want to install this version of atom? [quit]: " choice
    if [ "$choice" = "yes" ] || [ "$choice" = "y" ] || [ "$choice" = "Yes" ] || [ "$choice" = "Y" ]; then
        update_atom
    else
        exit 0
    fi
}

function update_atom(){
    if [[ $(grep -i ubuntu /etc/os-release) ]]; then
        # header
        std_message "upgrading atom for ${ORANGE}${BOLD}ubuntu${rst}${reset}" "INFO"
        # set to ubuntu format
        TARGET='atom-amd64.deb'
        # rm old file, if exists
        if [ ! -f $TMPDIR/$TARGET ]; then
            # download atom, latest
            curl -L https://atom.io/download/deb > $TMPDIR/$TARGET
        fi
        # Update atom from downloaded
        sudo dpkg --install $TMPDIR/$TARGET

    elif [[ $(grep -i redhat /etc/os-release) ]]; then
        # header
        std_message "upgrading atom for ${bold}${RED}redhat${reset}${rst}" "INFO"
        # set to rh format
        TARGET='atom.x86_64.rpm'
        # rm old file, if exists
        if [ ! -f $TMPDIR/$TARGET ]; then
            # download atom, latest
            curl -L https://atom.io/download/rpm > $TMPDIR/$TARGET
        fi
        # Update atom from downloaded
        sudo rpm -Uvh $TMPDIR/$TARGET

    else
        echo -e "\n unidentified distro detected. Aborting \n"
        exit 1
    fi

    # check installation/ upgrade success
    verify_update


}

function verify_update(){
    ## validates install/ upgrade ##
    if [ ${atom_io} ]; then
        std_message "${ORANGE}atom${reset} update completed" "INFO"
    else
        std_message "Unknown Failure.  ${ORANGE}atom${reset} not installed or upgraded correctly." "WARN"
    fi
    return 0
}


function prerun(){
    # color library
    if [[ -f "$config_path/colors.sh" ]]; then
        . "$config_path/colors.sh"

    elif [[ -f "/home/$SUDO_USER/.config/bash/colors.sh" ]]; then
        . "/home/$SUDO_USER/.config/bash/colors.sh"

    else
        printf -- '[%s]: %s\n' "WARN" "$config_path/colors.sh Dependency missing. Exit"
        exit 1
    fi

    # function library
    if [[ -f "$config_path/std_functions.sh" ]]; then
        . "$config_path/std_functions.sh"

    elif [[ -f "/home/$SUDO_USER/.config/bash/std_functions.sh" ]]; then
        . "/home/$SUDO_USER/.config/bash/std_functions.sh"

    else
        printf -- '[%s]: %s\n' "WARN" "$config_path/std_functions.sh Dependency missing. Exit"
        exit 1
    fi
    return 0
}


# --- main ------------------------------------------------------------


# source external sources
prerun

# formatting
RED=$(tput setaf 1)
bcy=$(echo -e ${a_brightcyan})
gbl=$(echo -e ${ITALIC}${a_bluegray})
rst=${RESET}

parse_parameters $@


if [ "$UPDATE_ATOM" = "true" ]; then

    if [ $EUID -ne 0 ]; then
        std_message "You must run this operation as root or sudo privileges. Exit" "WARN"
        exit 1

    elif [ "$VERSION" = "latest" ]; then
        pre_update

    elif _valid_version "$VERSION"; then
        pre_update "$VERSION"
    fi
elif [ $SHOW_VERSION ]; then
    reported_version $LATEST_URL
fi

# <-- end -->
exit 0
