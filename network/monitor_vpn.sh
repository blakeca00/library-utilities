#!/usr/bin/python
import boto.ec2.cloudwatch
import boto.vpc

AWS_Regions=["us-east-1","us-west-2","us-west-1","eu-west-1","ap-southeast-1","ap-northeast-1","ap-southeast-2","sa-east-1","eu-central-1"]
#AWS_Regions=boto.ec2.regions()
CloudWatch_Region="us-east-1"

cw = boto.ec2.cloudwatch.connect_to_region(CloudWatch_Region)
for region in AWS_Regions:
  vpcconn = boto.vpc.connect_to_region(region)
  vpns = vpcconn.get_all_vpn_connections()
  for vpn in vpns:
    if vpn.state == "available":
      active_tunnels = 0
      if vpn.tunnels[0].status == "UP":
        active_tunnels+=1
      if vpn.tunnels[1].status == "UP":
        active_tunnels+=1
      print vpn.id+" has "+str(active_tunnels)+" active tunnels!"
      cw.put_metric_data("VPNStatus", vpn.id, value=active_tunnels, dimensions={'VGW':vpn.vpn_gateway_id, 'CGW':vpn.customer_gateway_id})