#!/usr/bin/env bash

# global vars
pkg=$(basename $0)

# Formatting
blue=$(tput setaf 4)
cyan=$(tput setaf 6)
green=$(tput setaf 2)
purple=$(tput setaf 5)
red=$(tput setaf 1)
white=$(tput setaf 7)
yellow=$(tput setaf 3)
gray=$(tput setaf 008)
lgray='\033[0;37m'      # light gray
dgray='\033[1;30m'       # dark gray
reset=$(tput sgr0)
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`


### Globals ###
AWS=$(which aws)
PRINTF=$(which printf)
TEMP_FILE="~/Downloads/$NAME.tmp"


#<-- function declarations -->

# indent
function indent02() { sed 's/^/  /'; }
function indent10() { sed 's/^/          /'; }
function indent15() { sed 's/^/               /'; }
function indent18() { sed 's/^/                  /'; }


function help_menu(){
    echo -e "\n${lgray}${BOLD}Input format${UNBOLD}${reset}:\n" | indent02
    echo -e "$ ${BOLD}$pkg${UNBOLD} -a <${lgray}ACCTFILE${reset}> -r <${lgray}REGIONFILE${reset}> -s <${lgray}STACKNAME${reset}>\n" | indent10
}


function parse_parameters(){
    if [[ ! $@ ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -a | --accounts)
                    ACCTFILE=$2
                    shift 2
                    ;;
                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;
                -r | --regions)
                    REGIONFILE=$2
                    shift 2
                    ;;
                -s | --stack)
                    STACKNAME=$2
                    shift 2
                    ;;
                *)
                    help_menu
                    msg="Unrecognized argument [ $1 ] given. Exiting (code 1)"
                    echo "$msg"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <<-- end function parse_parameters -->>
}

# -- MAIN ---------------------------------------------------------------------

parse_parameters $@

# validate input parameters
if [ $DBUG ]; then
    echo -e "\nACCTFILE is: $ACCTFILE"
    echo -e "REGIONFILE is: $REGIONFILE"
    echo -e "STACKNAME is: $STACKNAME"
fi


echo -e "\nMetadata for Stack:  $STACK_NAME" | indent10
echo -e "____________________________________________\n" | indent02


for alias in $(cat $ACCTFILE); do
        for region in $(cat $REGIONFILE); do
            # STACK_EXISTS="$(aws cloudformation describe-stacks --stack-name $STACKNAME --profile "gcreds-$alias" --region $region 2>/dev/null | grep Stacks | awk -F ':' '{print $1}')"
            STACK_EXISTS="$(aws cloudformation describe-stacks --stack-name $STACKNAME \
                        --profile "gcreds-$alias" \
                        --region $region 2>/dev/null | grep Stacks | awk -F ':' '{print $1}')"

            if [ $STACK_EXISTS ]; then
                $PRINTF '\n  StackName: %s\t\tAccount: %s' "$STACKNAME" "$alias"
                echo -e ""
                echo -e "Audit Region ${lgray}${BOLD}$region${UNBOLD}${reset}\n"

                # get detailed metadata on stack
                $AWS cloudformation describe-stacks \
                    --stack-name $STACK_NAME \
                    --profile "gcreds-$alias" \
                    --region $region > $TEMP_FILE

                LAST_UPDATE=$(jq -r .Stacks[].LastUpdatedTime $TEMP_FILE)
                CREATE_TIME=$(jq -r .Stacks[].CreationTime $TEMP_FILE)
                echo -e "CreateTime: $CREATE_TIME"
                echo -e "Last Updated: $LAST_UPDATE\n"

            else
                echo -e "\n$STACKNAME already exists in region $region in account $alias"
                STACK_EXISTS=''    # reset flag
            fi
    done
done

# clean up
if [ -e $TEMP_FILE ]; then rm $TEMP_FILE; fi
#<-- end -->
exit 0
