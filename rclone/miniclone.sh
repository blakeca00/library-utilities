#!/usr/bin/env bash

###############################################################
#
#   PURPOSE:  TIG GOOGLE DRIVE BACKUP SCRIPT
#   AUTHOR:   BLAKE HUBER
#   VERSION:  FALL 2024
#
###############################################################


# globals
pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
clear=$(command -v clear)
_VERSION="1.3"

# formatting
LOG_FILE=$HOME/logs/rclone.log
source $pkg_path/std_functions.sh
source $pkg_path/colors.sh
frame=$(echo -e ${brightblue})
reset=$(tput sgr0)
bodytext=$(echo -e ${reset}${a_wgray})
btext=$(echo -e ${reset}${a_wgray})
sp="${frame}|${bodytext}"
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`
#
# ansi bright colors
a_brightgreen='\033[38;5;95;38;5;46m'                  # ansi brightgreen
wb=${BOLD}${white}                                     # white bold
lbkt="$(echo -e ${cyan}${BOLD})[${reset}"              # left arrow bracket
rbkt="$(echo -e ${cyan}${BOLD})]${reset}"              # right arrow bracket
lar="$(echo -e ${lgray}${BOLD})<${reset}"              # left arrow bracket
rar="$(echo -e ${lgray}${BOLD})>${reset}"              # right arrow bracket
vb="$(echo -e ${cyan}${BOLD})|${reset}"                # separator bar
orange=$(echo -e ${a_orange})                            # highlight color

ORIGIN_DIR=$(pwd)
TMP_ROOT='/home/blake/Downloads'

# Static Portfolio dirs:
LOCAL_STATIC_SOURCE="$TMP_ROOT/gdrive"
LOCAL_STATIC_DESTINATION="/home/blake/Documents/Trading/STATIC_PORTFOLIO_DOCUMENTATION"

# TIG dirs:
LOCAL_TIG_SOURCE="$TMP_ROOT/TIG"
LOCAL_TIG_DESTINATION="/home/blake/Documents/Trading/TIG"

# Set excecutables
rclone_bin=$(command -v rclone)
rsync_bin=$(command -v rsync)
BROADCAST=false

# aws variables
S3_BUCKET='backup-gdrive'
REGION='us-east-2'

# error codes
E_DEPENDENCY=1
E_BADARG=8                  # exit code if bad input parameter
                            # given profilename from local awscli configuration

# --- declarations ------------------------------------------------------------


function help_menu(){
    cat <<EOM

                            ${wb}miniclone.sh${btext} command help

  ${title}DESCRIPTION${btext}

          Backup TIG and Static Portfolio Google Drive  file  repositories
          to the local machine.  Backup  occurs via rclone for  extraction
          from Google Drive followed by rsync to local drives. Rclone sync
          is used to create an incremental  differential copy on the local
          drive staging areas to which rsync is applied.

  ${title}SYNOPSIS${btext}

        $ sh ${orange}$pkg${reset}  ${lbkt} --tig-backup <value> ${rbkt} ${cyan}||${btext} ${lbkt} --static-portfolio ${rbkt}

                       [-a | --all-jobs ]
                       [-c | --clean-tempdir ]
                       [-s | --static-portfolio ]
                       [-t | --tig-backup ]
                       [-h | --help  ]
                       [-V | --version ]

  ${title}OPTIONS${btext}

        ${title}-c${btext}, ${title}--clean-tempdir${btext}:  Clean & recreate all temp directories.

        ${title}-p${btext}, ${title}--prerequisites${btext}:  Test if required dependent apps are present.

        ${title}-t${btext}, ${title}--tig-backup${btext}  <value> :  When provided, parameter uses branch
            name provided for  <value> as the baseline code reference
            instead of the master branch  (DEFAULT branch reference).

        ${title}-s${btext}, ${title}--static-portfolio${btext}: If provided, $pkg backs up the
            Google Drive repository for the Static Porfolio
            reference file archive.

        ${title}-d${btext}, ${title}--debug${btext}:  Enable additional debug logging.

        ${title}-h${btext}, ${title}--help${btext}:  Display this help menu.

        ${title}-V${btext}, ${title}--version${btext}:  Show $pkg version & license information.

EOM
    #
    # <-- end function help_menu -->
}


function _clean_tempdir(){
    ##
    ##  Clean out temp directories located at:
    ##      - ~/Downloads/gdrive
    ##      - ~/Downloads/TIG
    local static_tempdir="Downloads/gdrive"
    local tig_tempdir="Downloads/TIG"

    # Static Portfolio tempdir
    if [[ -e "$HOME/$static_tempdir" ]]; then
        rm -fr "$HOME/$static_tempdir" || std_error_exit "tempdir for Static Portfolio not found. Exiting."
        mkdir "$HOME/$static_tempdir"
        std_message "tempdir ${a_orange}$HOME/$static_tempdir${reset} for Static Portfilio cleaned and recreated" "OK"
    else
        mkdir "$HOME/$static_tempdir"
        std_warn "Existing tempdir ${a_orange}$HOME/$static_tempdir${reset} not found. Created."
    fi

    # TIG tempdir
    if [[ -e "$HOME/$tig_tempdir" ]]; then
        rm -fr "$HOME/$tig_tempdir" || std_error_exit "tempdir for Static Portfolio not found. Exiting."
        mkdir "$HOME/$tig_tempdir"
        std_message "tempdir ${a_orange}$HOME/$tig_tempdir${reset} for TIG cleaned and recreated" "OK"
    else
        mkdir "$HOME/$tig_tempdir"
        std_warn "Existing tempdir ${a_orange}$HOME/$tig_tempdir${reset} not found. Created."
    fi
    return 0
    #
    # <-- end function _clean_tempdir -->
}


function display_version(){
    ## Displays version number when parameter given ##
    local version="$_VERSION"

    printf -- "\n\t${a_brightgreen}Google Drive${reset} Backup Script, version: ${a_orange}${BOLD}$version${reset}\n\n"
    #
    # <-- end function display_version -->
}


function _set_remote(){
    ## set the remote for STATIC portfolio based on hostname ##

    if [[ $(hostname | grep -i 'libra') ]]; then
        REMOTE='genericId'

    elif [[ $(hostname | grep -i 'aries') ]]; then
        REMOTE='gdrive'
    fi
    echo "$REMOTE"
    #
    # <-- end function set_remote -->
}


function verify_installation(){
    ## verifies installation of required dependency rclone
    local program="$1"
    local broadcast="$2"

    if [[ $(command -v "$program") ]]; then
        if [[ $broadcast = true ]]; then
            std_message "$program is installed." "INFO"
        fi
        return 0
    else
        std_message "$program not installed or not in your PATH. Exit." "WARN"
        return 1
    fi
}


function _prerequisites(){
    ## create log file if not found ##
    local quiet="$1"

    if [ ! -f "$LOG_FILE" ]; then
        std_message "Creating log file ${url}$LOG_FILE${reset} because it does not exist." "INFO" "$LOG_FILE"
        touch "$LOG_FILE"
    elif [[ $quiet = false ]]; then
        std_message "Found preexisting log file: ${url}$LOG_FILE${reset}" "INFO" "$LOG_FILE"
    fi

    ## create target temp directory if not found ##
    if [ ! -d "$LOCAL_STATIC_SOURCE" ]; then
        std_message "Creating SOURCE ${url}$LOCAL_STATIC_SOURCE${reset} directory." "INFO" "$LOG_FILE"
        mkdir -p "$LOCAL_STATIC_SOURCE"
    elif [[ $quiet = false ]]; then
        std_message "Found Pre-existing STATIC PORTFOLIO SOURCE ${url}$LOCAL_STATIC_SOURCE${reset} directory." "INFO" "$LOG_FILE"
    fi

    if [ ! -d "$LOCAL_TIG_SOURCE" ]; then
        std_message "Creating SOURCE ${url}$LOCAL_TIG_SOURCE${reset} directory." "INFO" "$LOG_FILE"
        mkdir -p "$LOCAL_TIG_SOURCE"
    elif [[ $quiet = false ]]; then
        std_message "Found Pre-existing TIG SOURCE ${url}$LOCAL_TIG_SOURCE${reset} directory." "INFO" "$LOG_FILE"
    fi

    ## verify dependencies ##
    if ! verify_installation 'rclone' "true" || ! verify_installation 'rsync' "true"; then
        std_warn "Failed access to required dependency rclone and/or rsync. Exit"
        exit 1
    fi
    #
    # <-- end function _prerequisites -->
}


function _upload(){
    ##
    ## upload objects to s3
    ##
    local object="$1"
    local prefix="$2"

    if [ "$prefix" ] && [ "$DBUG" ]; then
        # upload assets
        aws s3 cp $object s3://$BUCKET/${prefix}/$object --profile $PROFILE

    elif [ "$prefix" ]; then
        aws s3 cp $object s3://$BUCKET/${prefix}/$object --profile $PROFILE >/dev/null  2>&1

    elif [ ! "$prefix" ] && [ "$DBUG" ]; then
        # upload assets
        aws s3 cp $object s3://$BUCKET/$object --profile $PROFILE

    elif [ ! "$prefix" ]; then
        aws s3 cp $object s3://$BUCKET/$object --profile $PROFILE >/dev/null  2>&1
    fi
}


function _check_upload(){
    ##
    ## Upload validation with prefix in s3 path
    ##
    local object="$1"

    if [[ $(aws s3 ls s3://$BUCKET/$object --profile $PROFILE | awk '{print $1}') = "$(date +"%Y-%m-%d")" ]]; then
        std_message "Successful upload of ${obj}$object${rst} to Amazon S3 bucket ${bct}$BUCKET${rst}${div}${obj}$object${rst}" "OK"
    else
        std_message "Failed to upload ${obj}$object${rst} to Amazon S3 bucket ${bct}$BUCKET${rst}${div}${obj}$object${rst}" "WARN"
    fi
}


function file_count(){
    ## Count the num of files in target directory ##
    local target="$1"
    echo "$(find $target -type f | wc -l)"
    ##
    ## <-- end function file_count -->
}


function create_tgz_archive(){
    ## gzip and compress files in local temporary destination ##
    local tar_filepath="$1"     # full path including target tar filename
    local archive="$2"  # full path and dir name to archive
    tar -cvzf "$tar_filepath"  "$archive"
    #
    #< -- end function create_tgz_archive -->
}


function upload_2_aws(){
    ## uploads tgz compressed archive to aws ##
    local tar_filepath="$1"     # full path including target tar filename
    local filename=$(echo $tar_filepath | awk -F '/' '{print $NF}')
    local bucket="$2"
    #
    std_message "Uploading ${BOLD}$target${reset} to AWS S3 bucket ($S3_BUCKET)" "INFO"
    aws s3 cp "$tar_filepath" s3://$S3_BUCKET/"$filename"
    std_message "Upload to S3 complete. Deleting ${url}$target${reset} from local drive" "INFO"
    rm "$target"
    std_message "tgz archive ${url}$target${reset} deleted from local drive" "INFO"
    #
    # <-- end function upload_2_aws -->
}


function sync_from_remote(){
    ## calculates number of unique regions lambda functions found ##
    local from_remote="$1"
    local to_dir="$2"
    local rclone=$(command -v rclone)
    #
    std_message "Synchronizing files from rclone remote ${a_orange}$from_remote${reset} --> ${url}$to_dir.${reset}" "INFO" "$LOG_FILE"
    $rclone sync "$from_remote" "$to_dir"  --drive-export-formats=odt,ods,odp | tee -a "$LOG_FILE"
    std_message "Rclone Sync / Download completed."  "INFO" "$LOG_FILE"
    #
    # <-- end function sync_from_remote -->
}


function rsync_2local_target() {
    ## rsync fs from source dir to destination dir on local machine ##
    local source="$1"
    local destination="$2"
    #
    $rsync_bin -arv "$source"/ "$destination"/ --delete
    #
    # <-- end function rsync_2local_target -->
}


function report_elapsed_time(){
    local start="$1"
    local end=$(date +%s)
    local duration=$(($end - $start))

    if [[ $duration -le 60 ]]; then
        echo "$duration seconds"
    else
        echo "$(( $duration / 60 )) minutes"
    fi
    return 0
}


function static_portfolio_backup(){
    ## Back up of the Static long-term portfolio on Google Drive ##
    std_message "Starting backup of STATIC PORTFOLIO @ $(date +"%Y-%m-%d @ %H:%M")" "OK" "$LOG_FILE"

    start_time=$(date +%s)
    std_message "Finding remote for STATIC PORTFOLIO Backup..." "INFO" "$LOG_FILE"
    STATIC_PORTFOLIO_REMOTE=$(_set_remote)":"
    std_message "Remote for STATIC PORTFOLIO Backup set to: ${a_orange}$STATIC_PORTFOLIO_REMOTE${reset}" "INFO" "$LOG_FILE"
    std_message "Initial count of files in source ${url}$LOCAL_STATIC_SOURCE${reset} directory: ${a_orange}$(file_count "$LOCAL_STATIC_SOURCE")${reset} files" "INFO" "$LOG_FILE"

    FILE_COUNT=$(file_count $LOCAL_STATIC_DESTINATION)
    std_message "Initial count of files in destination ${url}$LOCAL_STATIC_DESTINATION${reset} directory: ${a_orange}$FILE_COUNT${reset} files" "INFO" "$LOG_FILE"

    # rclone Files for gdrive: remot to local temp directory
    cd "$LOCAL_STATIC_SOURCE" || std_error_exit "Failed to change to source directory $LOCAL_STATIC_SOURCE" "$E_DEPENDENCY"
    std_message "Begin rclone sync operation from Google Drive Remote (${bdwt}$STATIC_PORTFOLIO_REMOTE${reset}) --> ${url}$LOCAL_STATIC_SOURCE${reset}" "info" "$LOG_FILE"
    sync_from_remote "$STATIC_PORTFOLIO_REMOTE" "$LOCAL_STATIC_SOURCE"

    # Rsync Files for STATIC PORTFOLIO from source to destination
    std_message "Begin rsync of files from ${url}$LOCAL_STATIC_SOURCE${reset} location --> ${url}$LOCAL_STATIC_DESTINATION${reset}" "info" "$LOG_FILE"
    $rsync_bin -arv ./* $LOCAL_STATIC_DESTINATION/ --delete | tee -a "$LOG_FILE"

    # Closing Sum tally of files count in destination
    FILE_COUNT=$(file_count $LOCAL_STATIC_DESTINATION)
    std_message "Final count of files in destination ${url}$LOCAL_STATIC_DESTINATION${reset} directory: ${a_orange}$FILE_COUNT)${reset} files" "INFO" "$LOG_FILE"

    # Print Job Duration
    ET=$(report_elapsed_time "$start_time")
    std_message "Job completed @ ${a_orange}$(date +"%Y-%m-%d %H:%M:%S")${reset}" "INFO" "$LOG_FILE"
    std_message "Elapsed time for STATIC PORTFOLIO Backup is:  ${a_orange}$ET${reset}" "INFO" "$LOG_FILE"

    # rest prompt location
    cd "$ORIGIN_DIR"
    #
    #<--- end function static_portfolio_backup -->
}


function tig_backup(){
    ##
    ## TIG Backup ##

    std_message "Starting backup of TIG files @ $(date +"%Y-%m-%d @ %H:%M")" "OK" "$LOG_FILE"

    start_time=$(date +%s)
    TIG_REMOTE='TIG:'
    std_message "Remote for TIG Backup set to ${a_orange}$TIG_REMOTE${reset}" "INFO" "$LOG_FILE"

    FILE_COUNT=$(file_count $LOCAL_TIG_SOURCE)
    std_message "Initial count of files in source $LOCAL_TIG_SOURCE directory: ${a_orange}$FILE_COUNT${reset} files" "INFO" "$LOG_FILE"

    FILE_COUNT=$(file_count $LOCAL_TIG_DESTINATION)
    std_message "Initial count of files in destination $LOCAL_TIG_DESTINATION directory: ${a_orange}$FILE_COUNT${reset} files" "INFO" "$LOG_FILE"

    # begin backup operation

    cd "$LOCAL_TIG_SOURCE" || std_error_exit "Failed to change to source directory $LOCAL_TIG_SOURCE" $E_DEPENDENCY
    # rclone Files for TIG: remote to local temp directory
    std_message "Begin rclone sync operation from Google Drive Remote (${bdwt}$TIG_REMOTE${reset}) --> ${url}$LOCAL_TIG_SOURCE${reset}" "INFO" "$LOG_FILE"
    sync_from_remote "$TIG_REMOTE" "$LOCAL_TIG_SOURCE"

    # Rsync Files for TIG from source to destination
    std_message "Begin rsync of files from ${url}$LOCAL_TIG_SOURCE${reset} location --> ${url}$LOCAL_TIG_DESTINATION${reset}" "INFO" "$LOG_FILE"
    $rsync_bin -arv ./* $LOCAL_TIG_DESTINATION/ --delete | tee -a "$LOG_FILE"

    # Closing Sum Tally
    FILE_COUNT=$(file_count $LOCAL_TIG_DESTINATION)
    std_message "Final count of files in destination $LOCAL_TIG_DESTINATION directory: ${a_orange}$FILE_COUNT${reset} files" "INFO" "$LOG_FILE"

    # Print Job Duration
    ET=$(report_elapsed_time "$start_time")
    std_message "Job completed @ ${a_orange}$(date +"%Y-%m-%d %H:%M:%S")${reset}" "INFO" "$LOG_FILE"
    std_message "Elapsed time for TIG Backup is:  ${a_orange}$ET${reset}" "INFO" "$LOG_FILE"

    # rest prompt location
    cd "$ORIGIN_DIR"
    #
    # <-- end function tig_backup -->
}


function parse_parameters(){
    ##
    ##  Parse all command-line parameters
    ##

    local var command

    if [[ ! "$*" ]]; then

        help_menu
        exit 0

    else
        while [ $# -gt 0 ]; do
            case $1 in
                '-a' | '--all-jobs')
                    _prerequisites "false"
                    static_portfolio_backup
                    tig_backup
                    shift 1
                    ;;

                '-c' | '--clean-tempdir')
                    _clean_tempdir
                    shift 1
                    ;;

                '-h' | '--help')
                    help_menu
                    shift 1
                    ;;

                '-s' | '--static' | '--static-portfolio')
                    ### STATIC PORTFOLIO Backup ##
                    _prerequisites "false"
                    static_portfolio_backup
                    shift 1
                    ;;

                '-t' | '--tig' | '--tig-backup')
                    ## TIG Backup ##
                    _prerequisites "false"
                    tig_backup
                    shift 1
                    ;;

                '--test' )
                    # Test compression and upload to AWS logic
                    create_tgz_archive $TMP_ROOT/$(date +"%Y-%m-%d")_TIG.tgz "$LOCAL_TIG_SOURCE"
                    exit 0
                    upload_2_aws
                    ;;

                '-V' | '--version' )
                    # Display script version number
                    display_version
                    exit 0
                    ;;

                *)
                    std_warn "You must provide a valid parameter or None"
                    exit 1
                    ;;
            esac
        done
    fi
    #
    # <-- end function parse_parameters -->
}


#
# MAIN  ------------------------------------------------------------------
#

# run operation
parse_parameters "$@"

exit 0

##
