#!/usr/bin/env bash

# globals
pkg=$(basename $0)
pkg_path=$(cd $(dirname $0); pwd -P)
clear=$(command -v clear)

# formatting
LOG_FILE=$HOME/logs/rclone.log
source $pkg_path/std_functions.sh
source $pkg_path/colors.sh
frame=$(echo -e ${brightblue})
reset=$(tput sgr0)
bodytext=$(echo -e ${reset}${a_wgray})
sp="${frame}|${bodytext}"
#
BOLD=`tput bold`
UNBOLD=`tput sgr0`
#
# ansi bright colors
a_brightgreen='\033[38;5;95;38;5;46m'                  # ansi brightgreen
wb=${BOLD}${white}                                     # white bold
lbkt="$(echo -e ${cyan}${BOLD})[${reset}"              # left arrow bracket
rbkt="$(echo -e ${cyan}${BOLD})]${reset}"              # right arrow bracket
lar="$(echo -e ${lgray}${BOLD})<${reset}"              # left arrow bracket
rar="$(echo -e ${lgray}${BOLD})>${reset}"              # right arrow bracket
vb="$(echo -e ${cyan}${BOLD})|${reset}"                # separator bar
orange=$(echo -e ${orange})                            # highlight color

# set defaults
LOCAL_SOURCE_DEFAULT="/home/blake/Downloads/gdrive"
LOCAL_DESTINATION_DEFAULT="/home/blake/Documents/Trading/STATIC PORTFOLIO DOCUMENTATION"

# Set excecutables
rclone_bin=$(command -v rclone)
rsync_bin=$(command -v rsync)
BROADCAST=false

# error codes
E_BADARG=8                  # exit code if bad input parameter
                            # given profilename from local awscli configuration

# --- declarations ------------------------------------------------------------


function help_menu(){
    cat <<EOM
${bodytext}
                        Help Contents
                        -------------

  ${title}SYNOPSIS${bodytext}

        $  sh ${title}$pkg${bodytext}  --list ${vb} --verify  ${vb}  ${lbkt} --sync  ${vb}  --download ${rbkt} --remote

                   [-r | --remote   <${yellow}REMOTE${bodytext}> ]
                   [-c | --copy <${yellow}SOURCE${bodytext}> <${yellow}DESTINATION${bodytext}> ]
                   [-d | --download  ]
                   [-h | --help      ]
                   [-l | --list      ]
                   [-v | --verify    ]

  ${title}OPTIONS${bodytext}

      ${title}-c | --copy${bodytext}: Copy complete file set from <${yellow}REMOTE${bodytext}>. Must be used
                    in conjunction with --remote parameter.

      ${title}-d | --download${bodytext}: Copy complete file set from <${yellow}REMOTE${bodytext}> to /tmp
                    directory on local machine. Must be used with --remote parameter.

      ${title}-h | --help${bodytext}:  Print this help menu

      ${title}-l | --list${bodytext}:  List remotes available on this local machine.

      ${title}-r | --remote${bodytext}:  Remote file share (local or network location).

      ${title}-v | --verify${bodytext}: Verify installation of the rclone application
                  on local machine.

EOM
    #
    # <-- end function help -->
}


function set_remote(){
    ## set the remote for STATIC portfolio based on hostname ##

    std_message "Looking for remotes..." "INFO"

    if [[ $(echo $(hostname) | grep 'libra') ]]; then
        REMOTE='genericId'

    elif [[ $(echo $(hostname) | grep -i 'aries') ]]; then
        REMOTE='gdrive'
    fi
    echo "$REMOTE"
    #
    # <-- end function set_remote -->
}


function _prerequisites(){
    ## create log file if not found ##
    local quiet="$1"

    if [ ! -f "$LOG_FILE" ]; then
        std_message "Creating log file ${url}$LOG_FILE${reset} because it does not exist." "INFO" "LOG_FILE"
        touch "$LOG_FILE"
    elif [[ $quiet = false ]]; then
        std_message "Found preexisting log file: ${url}$LOG_FILE${reset}" "INFO" "LOG_FILE"
    fi

    ## create target temp directory if not found ##
    if [ ! -d "$LOCAL_SOURCE_DEFAULT" ]; then
        std_message "Creating ${url}$LOCAL_SOURCE_DEFAULT${reset} directory." "INFO" "LOG_FILE"
        mkdir -p "$LOCAL_SOURCE_DEFAULT"
    elif [[ $quiet = false ]]; then
        std_message "Found preexisting LOCAL_SOURCE_DEFAULT file: ${url}$LOCAL_SOURCE_DEFAULT${reset}" "INFO" "LOG_FILE"

    fi

    ## verify dependencies ##
    if ! verify_installation 'rclone' "false" || ! verify_installation 'rsync' "false"; then
        std_warn "Failed access to required dependency rclone and/or rsync. Exit"
        exit 1
    fi
    #
    # <-- end function _prerequisites -->
}


function file_count(){
    ## Count the num of files in target directory ##
    local target="$1"
    echo "$(find $target -type f | wc -l)"
    ##
    ## <-- end function file_count -->
}


function parse_parameters(){
    if [[ ! $@ ]]; then
        help_menu
        exit 0
    else
        while [ $# -gt 0 ]; do
            case $1 in
                -r | --remote)
                    REMOTE="$2"
                    shift 2
                    ;;

                -b | --backup)
                    OPERATION="COPY"
                    if [ "$2" ] && [ "$3" ]; then
                        SOURCE="$2"
                        DESTINATION="$3"
                        std_message "SOURCE: $SOURCE, DESTINATION:  $DESTINATION." "INFO"
                        std_message "Initial count of files in source $LOCAL_SOURCE_DEFAULT directory: ${a_orange}$(file_count "$LOCAL_SOURCE_DEFAULT")${reset} files" "INFO"
                        shift 3
                    else
                        std_error_exit "Both source and destation must be provided after the --copy parameter" "1"
                    fi
                    ;;

                -d | --download)
                    OPERATION="DOWNLOAD"
                    shift 1
                    ;;

                -l | --list)
                    OPERATION="LIST"
                    shift 1
                    ;;

                -h | --help)
                    help_menu
                    shift 1
                    exit 0
                    ;;

                -v | --verify)
                    verify_installation 'rclone' "true"
                    verify_installation 'rsync' "true"
                    _prerequisites "false"
                    exit 0
                    ;;

                *)
                    echo "Unknown parameter ($1). Exiting"
                    exit $E_BADARG
                    ;;
            esac
        done
    fi
    if [ $OPERATION = "DOWNLOAD" ] && [ ! "$REMOTE" ]; then
        std_error_exit "You must provide a remote fileshare location (--remote <fileshare>) from which to copy. Exit" "1"
        #std_error_exit "You cannot use --remote with the list operation. Exit" "1"
    fi
    #
    # <-- end function parse_parameters -->
}


function download_from_remote(){
    ## calculates number of unique regions lambda functions found ##
    local from_remote="$1"
    local to_localdir="/tmp"
    local rclone=$(command -v rclone)
    #
    std_message "Downloading from $from_remote to $to_localdir." "INFO"
    $rclone copy "$from_remote" "$to_localdir"
    std_message "Download completed."  "INFO"
    #
    # <-- end function download_from_remote -->
}


function rsync_2local_target() {
    ## rsync fs from source dir to destination dir on local machine ##
    local source="$1"
    local destination="$2"
    #
    $rsync_bin -arv "$source"/ "$destination"/ --delete
    #
    # <-- end function rsync_2local_target -->
}


function verify_installation(){
    ## verifies installation of required dependency rclone
    local program="$1"
    local broadcast="$2"

    if [[ $(command -v "$program") ]]; then
        if [[ $broadcast = true ]]; then
            std_message "$program is installed." "INFO"
        fi
        return 0
    else
        std_message "$program not installed or not in your PATH. Exit." "WARN"
        return 1
    fi
}

#
# MAIN  ------------------------------------------------------------------
#

parse_parameters $@

std_message "OPERATION IS:  $OPERATION" "INFO" "LOG_FILE"

_prerequisites "true"

# begin
if [ "$OPERATION" = 'BACKUP' ]; then
    if [ "$REMOTE" ]; then

        std_message "REMOTE passed as a parameter and set to: ${a_orange}$REMOTE${reset}" "INFO" "LOG_FILE"

    elif [ ! "$REMOTE" ]; then

        # find remote; otherwise, error msg & exit
        REMOTE=$(set_remote) || std_error_exit "Could not set remote for unknown reason. Exit"

        std_message "Found hostname ${a_bluegray}$(hostname)${reset}, REMOTE set to ${a_orange}$REMOTE${reset}" "INFO" "LOG_FILE"
    fi

elif [ $OPERATION = "LIST" ] && [ ! "$REMOTE" ]; then

    declare remotes=()

    count=1
    remotes=( $($rclone_bin listremotes) )

    for t in ${remotes[@]}; do
        printf -- '\t\t%s  %s\n' "Remote $count:" "$t"
        count=$(( $count + 1 ))
    done
fi

std_message "Initial count of files in source $LOCAL_SOURCE directory: ${a_orange}$(file_count "$LOCAL_DESTINATION")${reset} files" "INFO" "LOG_FILE"

exit 0

std_message "Beginning download.  Please wait..." "INFO" "LOG_FILE"
download_from_remote "$REMOTE"
std_message "Download complete." "INFO" "LOG_FILE"

elif [ $OPERATION = "LIST" ] && [ "$REMOTE" ]; then

    $rclone ls "$REMOTE"

elif [ $OPERATION = "COPY" ]; then

    std_message "Beginning Copy operation from $SOURCE --> $DESTINATION." "INFO"
    $rclone copy "$SOURCE" "$LOCAL_SOURCE_DEFAULT"
    rsync_2local_target "$LOCAL_SOURCE_DEFAULT" "$LOCAL_DESTINATION_DEFAULT"

fi

exit 0
