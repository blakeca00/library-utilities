* * *
# Utilities for use with rclone
* * *

1. `miniclone.sh`:  Bash script using rclone to backup object repositories hosted on Google Cloud.

2. `filecounts.py`:  Python 3 script for determining the age and contents of temporary file directories
                     used during back up operation in (1) above.


* * *
