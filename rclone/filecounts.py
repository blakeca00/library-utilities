#!/usr/bin/env python3


import os
import datetime
import sys
import subprocess
from pathlib import Path
from libtools import Colors

c = Colors()

# universal colors
acct = c.BOLD + c.BRIGHT_GREEN
text = c.BRIGHT_PURPLE
rd = c.RED + c.BOLD
yl = c.YELLOW + c.BOLD
fs = c.GOLD3
bd = c.BOLD
gn = c.BRIGHT_GREEN
title = c.BRIGHT_WHITE + c.BOLD
bcy = c.BRIGHT_CYAN
bdcy = c.CYAN + c.BOLD                  # bold blue
bdwt = c.BRIGHT_WHITE + c.BOLD          # white bold highlight color
bbc = bd + c.BRIGHT_CYAN
bbl = bd + c.BRIGHT_BLUE
highlight = bd + c.BRIGHT_BLUE
frame = gn + bd
btext = text + c.BOLD
bwt = c.BRIGHT_WHITE
bdwt = c.BOLD + c.BRIGHT_WHITE
ub = c.UNBOLD
rst = c.RESET

# Directories to be sourced for file counter
TARGET1 = '/home/blake/Downloads/TIG'
TARGET2 = '/home/blake/Downloads/gdrive'
TARGET3 = '/home/blake/Documents/Trading/TIG'
TARGET4 = '/home/blake/Documents/Trading/STATIC_PORTFOLIO_DOCUMENTATION'

line_divider = '\n' + gn + '_' * 80 + '\n' + rst


def file_count(directory):
    """
    Returns a count of the number of files in a directory
    """
    count = 0
    for root, dirs, files in os.walk(directory):
        for file in files:
            count = count + 1
    return count


def convert_dt_seconds(seconds):
    """Converts number of seconds to hours or days rounded to 1 decimal place"""
    days = seconds / 60 / 60 / 24
    if days < 1:
        return str(round(days * 24, 1)) + rst + '   hours'
    return str(int(days)) + rst + '   days'


def file_age_new(filepath, unit='seconds'):
    """
    Summary.

        Calculates file age in seconds

    Args:
        :filepath (str): path to file
        :unit (str): unit of time measurement returned.
            Valid values are:
                - second(s)
                - hour(s)
                - day(s)
    Returns:
        age (int)
    """
    ctime = os.path.getctime(filepath)
    dt = datetime.datetime.fromtimestamp(ctime)
    now = datetime.datetime.utcnow()
    delta = now - dt
    if unit in 'days':
        return round(delta.days, 2)
    elif unit in 'hours':
        return round(delta.seconds / 3600, 2)
    return round(delta.seconds, 2)


def filesystem_age(fname):
    """Returns the age in days of the file or folder given as a parameter"""
    try:
        st = os.stat(fname)
    except Exception:
        return 'NA'   # No filename given || age = 0 (new)
    else:
        dt_modified = datetime.datetime.fromtimestamp(st.st_mtime)
        # calc age
        now = datetime.datetime.now()
        return convert_dt_seconds((now - dt_modified).seconds)


def file_age(filepath, isoformat=True):
    """ Function to calc age of a file
        filepath (str):  path incl filename
    """
    f_obj = Path(filepath)
    mod_timestamp = f_obj.stat().st_mtime
    # convert to dd-mm-yyyy hh:mm:ss
    m_time = datetime.datetime.fromtimestamp(mod_timestamp)
    return m_time.isoformat() if isoformat is True else m_time


## -- START ------------------------------------------------------------------------------------- ##


directory_list = []

for num in range(5):
    directory_list.append('TARGET' + str(num))


print(line_divider)

####  TIG - File Counts & Age  ####

# TIG staging directory
fc = '{:,}'.format(file_count(TARGET1))
#age = filesystem_age_days(TARGET1)     # NOT USED ATM
age = file_age_new(TARGET1, 'days')

# results
print('\n' + title + 'TIG Staging Directory: ' + rst + c.URL + TARGET1 + rst)
print('TIG temp (source) directory file count: {}{} {:>10} {} files'.format('\t'.expandtabs(21), text, fc, rst))
print('TIG temp (source) directory age: {}{} {:>10} {}{}\n'.format('\t'.expandtabs(29), text, age, rst, 'days'))

# TIG official production directory
fc = '{:,}'.format(file_count(TARGET3))
#age = filesystem_age_days(TARGET3)     # NOT USED ATM
age = file_age_new(TARGET3, 'days')

# results
print('\n' + title + 'TIG Production Directory: ' + rst + c.URL + TARGET3 + rst)
print('TIG Production directory file count: {}{} {:>10} {} files'.format('\t'.expandtabs(24), text, fc, rst))
print('TIG Production directory age: {}{} {:>10} {}{}\n'.format('\t'.expandtabs(32), text, age, rst, 'days'))

print(line_divider)


####  STATIC PORTFOLIO - File Counts & Age  ####

# STATIC Portfolio staging directory - sort by modified time (newest first in the list)
fc = '{:,}'.format(file_count(TARGET2))
#age = filesystem_age_days(TARGET2)  # NOT USED ATM
age = file_age_new(TARGET2, 'days')

# results
print('\n' + title + 'STATIC PORTFOLIO Staging Directory: ' + rst + c.URL + TARGET2 + rst)
print('STATIC (source) directory file count: {}{} {:>10} {} files'.format('\t'.expandtabs(23), text, fc, rst))
print('STATIC (source) directory age: {}{} {:>10} {}{}\n'.format('\t'.expandtabs(31), text, age, rst, 'days'))

# STATIC Portfolio official production directory - sort by modified time (newest first in the list)
fc = '{:,}'.format(file_count(TARGET4))
#age = filesystem_age_days(TARGET4)   # NOT USED ATM
age = file_age_new(TARGET4, 'days')

# results
print('\n' + title + 'STATIC PORTFOLIO Production Directory: ' + rst + '\n' + c.URL + TARGET4 + rst)
print('STATIC Portfolio Production directory file count: {} {} {} {} files'.format('\t'.expandtabs(15), text, fc, rst))
print('STATIC Porfolio Production directory age: {}{} {:>10} {}{}\n'.format('\t'.expandtabs(20), text, age, rst, 'days'))
print(line_divider)


# end
sys.exit(0)
