* * *
# Utilities for parsing Sentiment Data
* * *

**Source Data**:  All data originates from [CNN's Fear & Greed Index](https://www.cnn.com/markets/fear-and-greed).

**Files**:

    * `sentiment.py`:  Python3 script for extracting daily market sentiment values from online sources.


* * *
