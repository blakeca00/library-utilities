#!/usr/bin/env python3

"""
Parse sentiment data files containing weekly market sentiment readings
"""

import os
import sys
from libtools import Colors

c = Colors()

# text formatting
ul = c.UNDERLINE
rst = c.RESET
tab3 = '\t'.expandtabs(3)
tab4 = '\t'.expandtabs(4)
tab8 = '\t'.expandtabs(8)


#
# -- Declarations ----------------------------------------------------------------------------------
#


def parse_records(record):
    """ Returns record parsed into dict format """
    date = record.split('_')[0]
    value = record.split('.')[0][-2:]
    return {'date': date, 'sentiment_value': value}


def print_results(result_list):
    """
    Prints parsed results to the terminal for inspection

        result_list (TYPE: list):  Contains dictionaries with the following
                                   schema:  {date : sentiment value}
    """
    header = '\n' + tab4 +  ul + '   Date   ' + rst + tab3 + ul + 'Sentiment' + rst
    print(header)
    for i in result_list:
        print(f"{tab4}{i['date']} | {i['sentiment_value']}")
    return True


#
# -- Start -----------------------------------------------------------------------------------------
#


# local directory containing all sentiment data files
TARGET1 = '/home/blake/Documents/Trading/TIG/RESOURCE_LIBRARY/Historical Charts and Data/Sentiment Data'

# list container for results
container = []

for root, dirs, files in os.walk(TARGET1):
    for f in files:
        if f.endswith('.png') and 'Statistics' not in f:
            parsed = parse_records(f)
            container.append(parsed)

# completed container must be sorted by date, not necessarily in chronological order
container_sorted = sorted(container, key=lambda x: x['date'])

# Output results to cli
print_results(container_sorted)


# -- end --

sys.exit(0)
